#include <algorithm>
#include "CTranspositionTable.h"

namespace fool
{

void CTranspositionTable::insertEntry(const uint64 hashKey, sint16 score, EScoreType scoreType, const int depth, const SMove & move)
{
    const int index = hashKey & TT_SIZE - 1; // % TT_SIZE
    int targetBucket {0};
    int lowestDepth {MAX_SEARCH_DEPTH};
    for (int bucket {0}; bucket < BUCKETS; bucket++)
    {
        if (0 == m_table[index][bucket].hashKey) //the slot is empty, store new entry here
        {
            targetBucket = bucket;
            break;
        }

        if (hashKey == m_table[index][bucket].hashKey)
        {
            if (depth > m_table[index][bucket].depth) //hashKey is the same and stored depth is less valuable - overwrite it
            {
                targetBucket = bucket;
                break;
            }
            else
            {
                return; //don't store the entry, the same with more valuable depth is alraedy stored
            }
        }

        if (m_table[index][bucket].depth < lowestDepth) //overwrite a slot with the least valuable depth
        {
            lowestDepth = m_table[index][bucket].depth;
            targetBucket = bucket;
        }
    }
    m_table[index][targetBucket].hashKey = hashKey;
    m_table[index][targetBucket].score = score;
    m_table[index][targetBucket].scoreType = scoreType;
    m_table[index][targetBucket].depth = depth;
    m_table[index][targetBucket].hashMoveFrom = move.from;
    m_table[index][targetBucket].hashMoveTo = move.to;
}

bool CTranspositionTable::isHashKeyValid(const uint64 hashKey)
{
    bool valid {false};
    m_currIndex = hashKey & TT_SIZE - 1; // % TT_SIZE
    for (int bucket {0}; bucket < BUCKETS; bucket++)
    {
        if (hashKey == m_table[m_currIndex][bucket].hashKey)
        {
            m_currBucket = bucket;
            valid = true;
            break;
        }
    }
    return valid;
}

bool CTranspositionTable::isScoreApplicable(const int depth, int alpha, int beta) const
{
    bool applicable {false};
    const STTEntry entry = m_table[m_currIndex][m_currBucket];
    if (entry.depth >= depth)
    {
        if (EXACT == entry.scoreType)
        {
            applicable = true;
        }
        else
        {
            if (LOWERBOUND == entry.scoreType)
            {
                alpha = std::max(alpha, static_cast<int>(entry.score));
            }
            else //UPPERBOUND == entry.scoreType
            {
                beta = std::min(beta, static_cast<int>(entry.score));
            }

            if (alpha >= beta)
            {
                applicable = true;
            }
        }
    }
    return applicable;
}

void CTranspositionTable::clear()
{
    for (int i = 0; i < TT_SIZE; i++)
    {
        m_table[i][0] = STTEntry();
        m_table[i][1] = STTEntry();
    }
}

}//namespace fool
