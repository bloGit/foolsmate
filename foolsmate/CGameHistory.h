/*Welcome!
  This is a simple and purely educational chess engine named "foolsmate".
  For best move search it uses the negamax algorithm with alpha-beta pruning + transposition table,
  iterative deepening, simple move ordering (hashmove, captures, killermove and history moves),
  and basic position score evaluation (material + PST).
  Move generation is bitboard-based with precalculated attack sets and magic numbers (for xray pieces).
  The only chess-specific source of knowledge: www.chessprogramming.org;

  In the code (identifiers/comments) you may find a few abbreviations/acronyms listed below:
     sq - square (on a chessboard), usually uint8 with a value <0, 63>
    occ - occupancy: the exact arrangement of chess pieces on the chessboard in a given moment;
          always expressed by a bitboard (uint64) or a set of bitboards, e.g. all pieces, only one type, only one color, etc
    pos - (game) position
    set - usually an attack set (or move set) as a bitboard, possibly other 'bitboard subset'
   1Fwd - normal pawn movement (a push) by 1 square forward
   2Fwd - a push by 2 squares forward (possible only from the initial position)
   EPSq - en passant square (the one on which the attaking pawn rests after the capture)
2FR/3FR - derived from Three-Fold-Repetition rule
          which causes draw after 3 identical positions (or just 2 for the sake of AI search)
 AI/CAI - (class) artificial intelligence - best move search component / CAISearchAgent object / 'AI search'
     TT - transposition table    
    LUT - look up table (CLookupData object, which serves to moveGen)
    PST - piece square tables (used for score evaluation)    
*/

#ifndef FOOLSMATE_CGAMEHISTORY
#define FOOLSMATE_CGAMEHISTORY

#include <array>
#include <utility> //std::pair
#include <cstring>
#include "CommonStuff.h"

namespace fool
{

enum EGamePhase: uint8
{
    MIDDLE_GAME = 0,
    END_GAME = 1
};

struct SPosition
{
    //occupancy bitbaords, first dimension for color (WHITE, BLACK), the second one for piece types;
    uint64_array_2x6 occ = {};

    //hash key of the position, it is a xor-constructed value based on zobrist hashing;
    //it represents (almost) uniquely a corresponding chess position and is much cheaper (storing/comparing)
    //than doing that upon an occupancy or whatever position's representation; utilized by TT and CGameHistory;
    uint64 hashKey {0};       

    //evaluation score of the position, it is returned in leaf-nodes of AI search (depth == 0) and further compared in negamax
    //processing to finally determine the best move in the root node based on the highest score returned by child-nodes;
    sint16 score {0};      

    //castling rights: bit indices 0 and 1 store long castling rights (for WHITE and BLACK);
    //indices 3 and 4 short ones accordingly;
    uint8 castlRights {0x0F}; //00001111: 4 least significant bits are set (all castling rights TRUE);

    //50-move count: if this variable reaches '100' the game is ended (draw) becasue 50 turns (100 halfmoves) have been done
    //and neither a piece was captured nor a pawn was moved;
    uint8 fiftyCount {0};
    
    //game phase to improve end game strength by using the end game PST versions;
    EGamePhase phase {MIDDLE_GAME};

#ifdef ICBCOM //consistency check for debug purpose
    bool operator==(const SPosition & rPosition) const
    {   
        bool equal = false;
        if (0 == std::memcmp(occ.data(), &rPosition.occ, sizeof(uint64) * 2 * 6)
           && hashKey == rPosition.hashKey
           && score == rPosition.score
           && castlRights == rPosition.castlRights
           && fiftyCount == rPosition.fiftyCount
           && phase == rPosition.phase)
        {
            equal = true;
        }
        return equal;
    }
    bool operator!=(const SPosition & rPosition) const { return !(*this == rPosition); }
#endif
};

class CGameHistory
{
    public:
    //************************ PUBLIC METHODS: **************************

#ifdef ICBCOM
    bool operator==(const CGameHistory & rHistory) const;
#endif

    //returns a number of up-to-now stored positions (made half-moves) in the game history;
    int size() const;

    //appends a position to the history;
    void pushPosition(const SPosition & pos);

    //extracts (and erases just by decrementing size) a position from the history;
    const SPosition & popPosition();

    //checks if a position represented by passed hashKey has already occured in the history (comparing with stored hash keys);
    //this version of repetition check is used by AI search because it is faster (and 3FR would be rather useless in that context);
    bool is2FRepetition(uint64 hashKey) const;

    //checks if a position represented by passed hashKey has already occured REPETITNION_LIMIT-1 times in the history;
    //used by the regular (current) game state for the sake of player's movement to fulfil the 3FR rule;
    bool is3FRepetition(uint64 hashKey) const;

    //XORs passed (current) occupancy with last stored (previous) one for each piece type to 'recall' which move has been made;
    //it is used to provide GUI with last move squares indices;
    std::pair<uint8, uint8> getLastMove(const uint64_array_2x6 & occ, EColor color) const;

    //XORs only pawns occupancies (passed one and previous one) then checks if it was 2Fwd move;
    //during undoMove() valid EP square must be recreated if 2Fwd turns out to be 'previous' in history;
    //if yes it returns EP square index to ultimately provide it to move generator (otherwise - 0);
    uint8 getEnPassantSquare(const uint64_array_2x6 & pOccupany, EColor color) const;


    private:
    //************************* DATA MEMBERS: ***************************
    
    static const int CAPACITY {128};

    //actual limit of repetitions is set to 4 (not 3 like in the rule name)
    //not to be too strict for a player / not to cause undesired/mistaken draw;
    static const int REPETITION_LIMIT {4};

    //actual history array;
    std::array<SPosition, CAPACITY> m_array;

    uint8 m_size {0};
};

inline void CGameHistory::pushPosition(const SPosition & pos)
{
    m_array[m_size++] = pos;
}

inline const SPosition & CGameHistory::popPosition()
{
    return m_array[m_size-- - 1];
}

}//namespace fool

#endif //FOOLSMATE_CGAMEHISTORY

