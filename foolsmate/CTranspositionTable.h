/*Welcome!
  This is a simple and purely educational chess engine named "foolsmate".
  For best move search it uses the negamax algorithm with alpha-beta pruning + transposition table,
  iterative deepening, simple move ordering (hashmove, captures, killermove and history moves),
  and basic position score evaluation (material + PST).
  Move generation is bitboard-based with precalculated attack sets and magic numbers (for xray pieces).
  The only chess-specific source of knowledge: www.chessprogramming.org;

  In the code (identifiers/comments) you may find a few abbreviations/acronyms listed below:
     sq - square (on a chessboard), usually uint8 with a value <0, 63>
    occ - occupancy: the exact arrangement of chess pieces on the chessboard in a given moment;
          always expressed by a bitboard (uint64) or a set of bitboards, e.g. all pieces, only one type, only one color, etc
    pos - (game) position
    set - usually an attack set (or move set) as a bitboard, possibly other 'bitboard subset'
   1Fwd - normal pawn movement (a push) by 1 square forward
   2Fwd - a push by 2 squares forward (possible only from the initial position)
   EPSq - en passant square (the one on which the attaking pawn rests after the capture)
2FR/3FR - derived from Three-Fold-Repetition rule
          which causes draw after 3 identical positions (or just 2 for the sake of AI search)
 AI/CAI - (class) artificial intelligence - best move search component / CAISearchAgent object / 'AI search'
     TT - transposition table    
    LUT - look up table (CLookupData object, which serves to moveGen)
    PST - piece square tables (used for score evaluation)    
*/

#ifndef FOOLSMATE_CTRANSPOSITIONTABLE
#define FOOLSMATE_CTRANSPOSITIONTABLE

#include "CMoveArray.h"

namespace fool
{

enum EScoreType: uint8
{
    EXACT      = 0,
    LOWERBOUND = 1,
    UPPERBOUND = 2
};

struct STTEntry
{
    //hashKey to be compared to an AI search node's hashKey, if they are equal it means that position's score
    //is already stored in TT and can be instantly returned instead of further tree expanding
    //(on condition that the position was saved at expected depth);
    uint64 hashKey {0};

    //saved score to be returned to AI if applicable;
    sint16 score {0};
    
    //denotes whether the position's score was stored as a lower bound (not all moves were probed)
    //upper bound (all were probed but none improved alpha) or exact (at least one improved alpha);
    EScoreType scoreType {EXACT};

    //depth at which this position was stored in TT; it is to be compared to depth of again encountered (current) position,
    //if current depth is lesser than stored one it means the stored score is possibly weaker
    //than the one to be examined at AI-expected depth (thus would break pure negamax depth-based evaluation)
    //so it is discarded (not returned) and current node's AI search is proceeded normally;
    uint8 depth {0};
    
    //hashMove coordinates; hashMove is the best (highest scored) move probed by AI last time at this position so it is worth
    //to order it first because it's high propability that as a good move it will cause a cut-off;
    //hashMove source square
    uint8 hashMoveFrom {255};

    //hashMove destination square
    uint8 hashMoveTo {255};    
};

class CTranspositionTable
{
    public:
    //************************ PUBLIC METHODS: **************************

    //adds entry to TT; if hashKey already exists in whatever bucket a position with more valuable depth is chosen to be stored;
    //if hashKey isn't stored the new one will be always added but it will overwrite the one with least valuable depth;
    void insertEntry(const uint64 hashKey, sint16 score, EScoreType scoreType, const int depth, const SMove & move);

    //tests hashKey stored in TT against one that falls in the same index during AI search (verifying it is really the same hash);
    bool isHashKeyValid(const uint64 hashKey);

    //tests position's depth stored in TT against the one of current node;
    bool isScoreApplicable(const int depth, int alpha, int beta) const;

    sint16 getScore() const;
    SMove getHashMove() const;
    void clear();


    private:
    //************************* DATA MEMBERS: ***************************

    //a power of 2 which apparently performs best in tests with depth == 9;
    static const int TT_SIZE {524288};

    //2-way set associative table performs better than a plain array;
    static const int BUCKETS {2};    
    
    //actuall TT array;
    std::array< std::array<STTEntry, BUCKETS>, TT_SIZE> m_table;    

    //for convinient TT interface they hold entry's index corresponsing to a node being processed in negamax;
    int m_currIndex {0};
    int m_currBucket {0};
};

inline sint16 CTranspositionTable::getScore() const
{
    return m_table[m_currIndex][m_currBucket].score;
}

inline SMove CTranspositionTable::getHashMove() const
{
    const STTEntry entry = m_table[m_currIndex][m_currBucket];
    SMove hashMove;
    hashMove.from = entry.hashMoveFrom;
    hashMove.to = entry.hashMoveTo;
    return hashMove;
}

}//namespace fool

#endif //FOOLSMATE_CTRANSPOSITIONTABLE



 //p:    49157
 //2:    65536
 //p:    98299
 //2:   131072 <<
 //p:   196613 <<
 //2:   262144
 //p:   393209 << 
 //2:   524288 <<
 //p:   786433
 //2:  1048576
 //p:  1572869
 //2:  2097152
 //p:  3145721
 //2:  4194304
 //p:  6291449
 //2:  8388608

