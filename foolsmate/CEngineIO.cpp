#include <array>
#include "CEngineIO.h"

/**************************************************************************************/
/********************************* DLL ENTRY FUNCTIONS: *******************************/
/**************************************************************************************/

namespace
{
    std::unique_ptr<fool::CEngineIO> g_pChessGame;
}

extern "C" __declspec(dllexport) void foolsmate_createGame()
{
    g_pChessGame = std::make_unique<fool::CEngineIO>();
}

extern "C" __declspec(dllexport) void foolsmate_deleteGame()
{
    g_pChessGame = nullptr;
}

extern "C" __declspec(dllexport) void foolsmate_registerCallback_redraw( void funPtr(fool::SGUISquare[64]) )
{
    g_pChessGame->registerCallback_redraw(funPtr);
}

extern "C" __declspec(dllexport) void foolsmate_resetGame()
{
    return g_pChessGame->resetGame();
}

extern "C" __declspec(dllexport) bool foolsmate_movePiece(int from, int to)
{
    return g_pChessGame->makeMove(from, to);
}

extern "C" __declspec(dllexport) void foolsmate_makeBestMovement(int searchDepth)
{
    g_pChessGame->makeBestMove(searchDepth);
}

extern "C" __declspec(dllexport) void foolsmate_takeBackMove()
{
    g_pChessGame->takeBackMove();
}

extern "C" __declspec(dllexport) void foolsmate_setCustomPosition(fool::uint8* ptr, fool::EColor color)
{
    g_pChessGame->setCustomPosition(ptr, color);
}

extern "C" __declspec(dllexport) void foolsmate_choosePiece(fool::EType piece)
{
    //not finished
}

extern "C" __declspec(dllexport) void foolsmate_quitGame(fool::EGameResult result)
{
    //not finished
}

/*****************************************************************************************/
/***************************** CEngineIO CLASS IMPLEMENTATION: ***************************/
/*****************************************************************************************/

namespace fool
{

CEngineIO::CEngineIO()
{    
    m_pGameState->setStartPosition();
}

template<typename F> void CEngineIO::registerCallback_redraw(F funPtr)
{
    m_redraw = funPtr;
    redrawGUI();
}

template<typename F> void CEngineIO::registerCallback_gameOver(F funPtr)
{
    m_gameOver = funPtr;
}

template<typename F> void CEngineIO::registerCallback_promotion(F funPtr)
{
    m_promotion = funPtr;
}

void CEngineIO::resetGame() //do testu na GUI
{
    m_bestMoveSeeker.abortSearch();    
    m_pGameState = std::make_unique<CGameState>(); //'recreate' the gameSate to re-init all data members
    m_pGameState->setStartPosition();    
    redrawGUI();
}

bool CEngineIO::makeMove(int from, int to)
{
    m_bestMoveSeeker.abortSearch();
    bool isCorrect {false};
    if (m_pGameState->move(from, to))
    {
        redrawGUI();

        //perform the callback to notify GUI about promotion
        if (m_pGameState->isPromotion())
        {
            m_promotion();
            redrawGUI();  //redraw once again to display newly chosen piece
        }

        //perform the callback to notify GUI about game over
        EGameResult gameResult {m_pGameState->getGameResult()};
        if (gameResult != IN_PROGRESS)
        {            
            m_pGameState = nullptr;
            m_gameOver(gameResult);
        }

        isCorrect = true; //wszystko powyzej z callback'ami wymaga gruntownch test�w z GUI
    }
    return isCorrect;
}

void CEngineIO::takeBackMove()
{
    if (!m_pGameState->isPromotion() && IN_PROGRESS == m_pGameState->getGameResult())
    {
        m_bestMoveSeeker.abortSearch();
        m_pGameState->undoMove();        
        redrawGUI();        
    }         
}

void CEngineIO::choosePiece(EType piece)
{
    if ( m_pGameState->isPromotion()  &&
         (BISHOP == piece || ROOK == piece ||
          KNIGHT == piece || QUEEN == piece) )
    {
        m_pGameState->choosePiece(piece);
    }
}

void CEngineIO::quitGame(EGameResult result) //do testu na GUI
{    
    if ( IN_PROGRESS == m_pGameState->getGameResult()  &&
         (WHITE_WINS == result || BLACK_WINS == result || DRAW == result) )
    {
        m_pGameState = nullptr;
        m_gameOver(result);  //perform the callback to handle GUI game-over        
    }
}

void CEngineIO::setCustomPosition(const uint8* const ptr, EColor colorToMove)
{
    if (WHITE == colorToMove || BLACK == colorToMove)
    {
        m_bestMoveSeeker.abortSearch();
        std::array<SGUISquare, 64> layout;        
        for (int i {0}, k {0}; i < 64; i++) //'deserialize' input array to properly fill SGUISquare array
        {
            layout[i].type = static_cast<EType>(ptr[k++]);
            layout[i].color = static_cast<EColor>(ptr[k++]);
            if (layout[i].type > NONE || layout[i].color > BLACK)
            {
                throw "setCustomPosition: INVALID piece/square passed!";
            }
        }
        m_pGameState = std::make_unique<CGameState>(); //'recreate' the gameSate to re-init all data members
        m_pGameState->setPositionFromLayout(layout, colorToMove);
        redrawGUI();
    }
}

void CEngineIO::makeBestMove(int searchDepth)
{        
    if (searchDepth > 0 && searchDepth < MAX_SEARCH_DEPTH &&
        IN_PROGRESS == m_pGameState->getGameResult()) //!m_pGameState->isPromotion()
    {        
        const SMove move = m_bestMoveSeeker.runNegamax(searchDepth, *m_pGameState);
        if (!m_bestMoveSeeker.isSearchAborted())
        {
            makeMove(move.from, move.to);
        }
    }
}

void CEngineIO::redrawGUI()
{    
    std::array<SGUISquare, 64> layoutForGUI;

    //update last move squares (it allows GUI to colorize the last move squares: from/to)
    const auto move = m_pGameState->getLastMove();
    for (int i {0}; i < 64; i++)
    {
        layoutForGUI[i].lastMove = (move.first == i || move.second == i);
    }

    //update destination squares (legal moves) - GUI holds own version of them for better responsiveness
    const auto moves = m_pGameState->getLegalMoves();
    for (int i {0}; i < moves.size(); i++)
    {
        layoutForGUI[moves[i].from].destSquares |= 1ULL << moves[i].to;
    }
    
    //update the layout of pieces (the chessboard "pattern")
    for (int type {0}; type < NONE; type++)
    {
        uint64 occ {m_pGameState->getOccupancy()[WHITE][type]};
        while (occ != 0)
        {
            uint32 sq {bitScanAndReset(occ)};
            layoutForGUI[sq].type = static_cast<EType>(type);
            layoutForGUI[sq].color = WHITE;            
        }

        occ = m_pGameState->getOccupancy()[BLACK][type];
        while (occ != 0)
        {
            uint32 sq {bitScanAndReset(occ)};
            layoutForGUI[sq].type = static_cast<EType>(type);
            layoutForGUI[sq].color = BLACK;            
        }
    }

    m_redraw(layoutForGUI.data()); //perform GUI callback
}

}//namespace fool


