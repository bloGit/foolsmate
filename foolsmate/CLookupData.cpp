#include "CLookupData.h"
#include <cmath> //std::abs
#include <random> //std::mt19937 / std::uniform_int_distr
#include <iostream> //std::cout (printBitboard)

namespace fool
{

CLookupData::CLookupData()
{
    initializeLUT();
    assignFunctionPointers();
}

/*********************************************************************************************************************/
/************************************************* PRIVATE METHODS: **************************************************/
/*********************************************************************************************************************/

uint64 CLookupData::pawn_generateAttackMask(const uint8 sq, const EColor color) const
{
    uint64 attackMask {0};
    int x {sq & 7};
    int y {sq >> 3};
    if (x + 1 < 8)
    {
        attackMask |= 1ULL << (x + 1 + (WHITE == color ? y + 1 : y - 1) * 8);
    }
    if (x - 1 >= 0)
    {
        attackMask |= 1ULL << (x - 1 + (WHITE == color ? y + 1 : y - 1) * 8);
    }
    return attackMask;
}

uint64 CLookupData::knight_generateAttackMask(const uint8 sq) const
{
    int x[8];
    int y[8];
    x[0] = (sq & 7) + 1;   y[0] = (sq >> 3) + 2;
    x[1] = (sq & 7) + 1;   y[1] = (sq >> 3) - 2;
    x[2] = (sq & 7) + 2;   y[2] = (sq >> 3) + 1;
    x[3] = (sq & 7) + 2;   y[3] = (sq >> 3) - 1;
    x[4] = (sq & 7) - 1;   y[4] = (sq >> 3) + 2;
    x[5] = (sq & 7) - 1;   y[5] = (sq >> 3) - 2;
    x[6] = (sq & 7) - 2;   y[6] = (sq >> 3) + 1;
    x[7] = (sq & 7) - 2;   y[7] = (sq >> 3) - 1;

    uint64 attackMask {0};
    for (int i {0}; i < 8; i++)
    {
        if (x[i] >= 0 && x[i] < 8 && y[i] >= 0 && y[i] < 8)
        {
            attackMask |= 1ULL << (x[i] + y[i] * 8);
        }
    }
    return attackMask;
}

uint64 CLookupData::king_generateAttackMask(const uint8 sq) const
{
    uint64 attackMask {0};
    const int x {sq & 7};
    const int y {sq >> 3};
    if (x + 1 < 8)
    {
        attackMask |= 1ULL << (x + 1 + y * 8);
    }
    if (x - 1 >= 0)
    {
        attackMask |= 1ULL << (x - 1 + y * 8);
    }
    if (y + 1 < 8)
    {
        attackMask |= 1ULL << (x + (y + 1) * 8);
    }
    if (y - 1 >= 0)
    {
        attackMask |= 1ULL << (x + (y - 1) * 8);
    }
    if ((x + 1 < 8) & (y + 1 < 8))
    {
        attackMask |= 1ULL << (x + 1 + (y + 1) * 8);
    }
    if ((x - 1 >= 0) & (y + 1 < 8))
    {
        attackMask |= 1ULL << (x - 1 + (y + 1) * 8);
    }
    if ((x + 1 < 8) & (y - 1 >= 0))
    {
        attackMask |= 1ULL << (x + 1 + (y - 1) * 8);
    }
    if ((x - 1 >= 0) & (y - 1 >= 0))
    {
        attackMask |= 1ULL << (x - 1 + (y - 1) * 8);
    }
    return attackMask;
}

void CLookupData::rook_generateMasks()
{
    for (int sq {0}; sq < 64; sq++)
    {
        uint64 attackMask {0};
        for (int i {1}; i < 7; i++)
        {
            attackMask |= 1ULL << (i + (sq >> 3) * 8);
            attackMask |= 1ULL << ((sq & 7) + i * 8);
        }
        attackMask &= ~(1ULL << sq); //clear rook's own bit
        m_rookAttackMask[sq] = attackMask;
    }
}

uint64 CLookupData::rook_generateOccupancyVariant(const uint8 sq, const int variantNum) const
{
    uint64 occ {0};
    for (int k {0}, i = 0; i < 64; i++)
    {
        if (m_rookAttackMask[sq] >> i & 1ULL)
        {
            occ |= (variantNum >> k++ & 1ULL) << i;
        }
    }
    return occ;
}

uint64 CLookupData::rook_generateAttackSet(const uint8 a_sq, const uint64 occ) const
{
    uint64 attackSet {0};
    for (int sq {a_sq + 1}; (sq & 7) > 0; sq++)
    {
        attackSet |= 1ULL << sq;
        if (occ & 1ULL << sq) break;
    }
    for (int sq {a_sq - 1}; (sq & 7) < 7; sq--)
    {
        attackSet |= 1ULL << sq;
        if (occ & 1ULL << sq) break;
    }
    for (int sq {a_sq + 8}; sq < 64; sq += 8)
    {
        attackSet |= 1ULL << sq;
        if (occ & 1ULL << sq) break;
    }
    for (int sq {a_sq - 8}; sq >= 0; sq -= 8)
    {
        attackSet |= 1ULL << sq;
        if (occ & 1ULL << sq) break;
    }
    return attackSet;
}

void CLookupData::bishop_generateMasks()
{
    for (int sq {0}; sq < 64; sq++)
    {
        uint64 attackMask {0};
        for (int i {sq + 9}; 1ULL << i & ~(A_FILE | H_FILE) && i < 56; i += 9)
        {
            attackMask |= 1ULL << i;
        }
        for (int i {sq - 9}; 1ULL << i & ~(A_FILE | H_FILE) && i > 7; i -= 9)
        {
            attackMask |= 1ULL << i;
        }
        for (int i {sq + 7}; 1ULL << i & ~(A_FILE | H_FILE) && i < 56; i += 7)
        {
            attackMask |= 1ULL << i;
        }
        for (int i {sq - 7}; 1ULL << i & ~(A_FILE | H_FILE) && i > 7; i -= 7)
        {
            attackMask |= 1ULL << i;
        }
        m_bishopAttackMask[sq] = attackMask;
    }
}

uint64 CLookupData::bishop_generateOccupancyVariant(const uint8 sq, const int variantNum) const
{
    uint64 occ {0};
    for (int k {0}, i {0}; i < 64; i++)
    {
        if (m_bishopAttackMask[sq] >> i & 1ULL)
        {
            occ |= (variantNum >> k++ & 1ULL) << i;
        }
    }
    return occ;
}

uint64 CLookupData::bishop_generateAttackSet(const uint8 a_sq, const uint64 occ) const
{
    uint64 attackSet {0};
    for (int sq {a_sq + 9}; (sq & 7) > 0 && sq < 64; sq += 9)
    {
        attackSet |= 1ULL << sq;
        if (occ & 1ULL << sq) break;
    }
    for (int sq {a_sq - 9}; (sq & 7) < 7 && sq >= 0; sq -= 9)
    {
        attackSet |= 1ULL << sq;
        if (occ & 1ULL << sq) break;
    }
    for (int sq {a_sq + 7}; (sq & 7) < 7 && sq < 64; sq += 7)
    {
        attackSet |= 1ULL << sq;
        if (occ & 1ULL << sq) break;
    }
    for (int sq {a_sq - 7}; (sq & 7) > 0 && sq >= 0; sq -= 7)
    {
        attackSet |= 1ULL << sq;
        if (occ & 1ULL << sq) break;
    }
    return attackSet;
}

void CLookupData::generateMagicNumbers(const bool rook)
{    
    std::random_device rd;
    std::mt19937 rng(rd()); //random number generator
    std::uniform_int_distribution<uint64> distribute;
    for (int sq {0}; sq < 64; sq++)
    {
        std::vector<uint64> occVariant(MAX_OCC_PER_SQUARE);
        std::vector<uint64> attackSet(MAX_OCC_PER_SQUARE);
        const int variants { 1 << __popcnt64((rook ? m_rookAttackMask : m_bishopAttackMask)[sq]) };
        for (int i {0}; i < variants; i++)
        {
            occVariant[i] = (rook ? rook_generateOccupancyVariant(sq, i) : bishop_generateOccupancyVariant(sq, i));
            attackSet[i] = (rook ? rook_generateAttackSet(sq, occVariant[i]) : bishop_generateAttackSet(sq, occVariant[i]));
        }

        uint64 magicNumber {0};
        bool fail {true};
        while (fail)
        {
            fail = false;            
            std::vector<uint64> used(MAX_OCC_PER_SQUARE, 0);
            magicNumber = distribute(rng) & distribute(rng) & distribute(rng);
            for (int i {0}; i < variants && !fail; i++)
            {
                const int index = static_cast<int>( occVariant[i] * magicNumber >> (rook ? 52 : 55) );
                if (0 == used[index])
                {
                    used[index] = attackSet[i];
                }
                else if (used[index] != attackSet[i])
                {
                    fail = true;
                }
            }
        }
        (rook ? m_rookMagicNumber : m_bishopMagicNumber)[sq] = magicNumber;
    }
}

uint64 CLookupData::generateXRayLine(const uint8 sq1, const uint8 sq2) const
{
    uint64 line {0};
    const int x1 {sq1 & 7};
    const int y1 {sq1 >> 3};
    const int x2 {sq2 & 7};
    const int y2 {sq2 >> 3};
    if (x1 == x2 && y1 < y2)
    {
        for (int y {y1}; y < y2; y++)
        {
            line |= 1ULL << (x1 + y * 8);
        }
    }
    else if (x1 == x2 && y1 > y2)
    {
        for (int y {y1}; y > y2; y--)
        {
            line |= 1ULL << (x1 + y * 8);
        }
    }
    else if (y1 == y2 && x1 < x2)
    {
        for (int x {x1}; x < x2; x++)
        {
            line |= 1ULL << (x + y1 * 8);
        }
    }
    else if (y1 == y2 && x1 > x2)
    {
        for (int x {x1}; x > x2; x--)
        {
            line |= 1ULL << (x + y1 * 8);
        }
    }
    else if (std::abs(x1 - x2) == std::abs(y1 - y2))
    {
        if (x1 < x2 && y1 < y2)
        {
            for (int x {x1}, y {y1}; x < x2; x++, y++)
            {
                line |= 1ULL << (x + y * 8);
            }
        }
        else if (x1 < x2 && y1 > y2)
        {
            for (int x {x1}, y {y1}; x < x2; x++, y--)
            {
                line |= 1ULL << (x + y * 8);
            }
        }
        else if (x1 > x2 && y1 < y2)
        {
            for (int x {x1}, y {y1}; x > x2; x--, y++)
            {
                line |= 1ULL << (x + y * 8);
            }
        }
        else if (x1 > x2 && y1 > y2)
        {
            for (int x {x1}, y {y1}; x > x2; x--, y--)
            {
                line |= 1ULL << (x + y * 8);
            }
        }
    }
    else if (std::abs(x1 - x2) == 2 && std::abs(y1 - y2) == 1
          || std::abs(x1 - x2) == 1 && std::abs(y1 - y2) == 2)
    {
        line |= 1ULL << (x1 + y1 * 8); //return knight's square too (if knight's move pattern is passed)
    }
    return line;
}

void CLookupData::initializeLUT()
{   
    rook_generateMasks();
    bishop_generateMasks();
    generateMagicNumbers(true);  //rook
    generateMagicNumbers(false); //bishop

    for (int sq {0}; sq < 64; sq++)
    {
        m_pawnAttackMask[sq][WHITE] = pawn_generateAttackMask(sq, WHITE);
        m_pawnAttackMask[sq][BLACK] = pawn_generateAttackMask(sq, BLACK);
        m_knightAttackMask[sq] = knight_generateAttackMask(sq);
        m_kingAttackMask[sq] = king_generateAttackMask(sq);

        for (int i {0}; i < (1 << __popcnt64(m_rookAttackMask[sq])); i++)
        {
            const uint64 occ { rook_generateOccupancyVariant(sq, i) };
            const uint64 attackSet { rook_generateAttackSet(sq, occ) };
            const int index = occ * m_rookMagicNumber[sq] >> 52;
            m_rookAttackSet[sq][index] = attackSet;
        }

        for (int i {0}; i < (1 << __popcnt64(m_bishopAttackMask[sq])); i++)
        {
            const uint64 occ { bishop_generateOccupancyVariant(sq, i) };
            const uint64 attackSet { bishop_generateAttackSet(sq, occ) };
            const int index = occ * m_bishopMagicNumber[sq] >> 55;
            m_bishopAttackSet[sq][index] = attackSet;
        }

        for (int sq2 {0}; sq2 < 64; sq2++)
        {
            m_line[sq][sq2] = generateXRayLine(sq, sq2);
        }
    }
}

void CLookupData::assignFunctionPointers()
{
    m_getAttackSet_fPtr[KNIGHT] = &CLookupData::getKnightAttackSet;
    m_getAttackSet_fPtr[BISHOP] = &CLookupData::getBishopAttackSet;
    m_getAttackSet_fPtr[ROOK]   = &CLookupData::getRookAttackSet;
    m_getAttackSet_fPtr[QUEEN]  = &CLookupData::getQueenAttackSet;
    m_getAttackSet_fPtr[KING]   = &CLookupData::getKingAttackSet;
}

void CLookupData::printBitboard(uint64 bitBoard) const
{
    std::cout << std::hex << bitBoard << std::dec << std::endl;
    for (int y {7}; y >= 0; y--)
    {
        uint8 rank { static_cast<uint8>(bitBoard >> y * 8) };
        for (int x {0}; x < 8; x++)
        {
            std::cout << (rank >> x & 1 ? "1" : ".") << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

}//namespace fool


