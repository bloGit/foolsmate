#include <random> //std::mt19937 + std::uniform_int_distr
#include "CGameState.h"

namespace fool
{

CGameState::uint64_array_2x6x64 CGameState::m_zobrist;
uint64 CGameState::m_blackToMove;
uint64 CGameState::m_WLCastling;
uint64 CGameState::m_WSCastling;
uint64 CGameState::m_BLCastling;
uint64 CGameState::m_BSCastling;
std::array<uint64, 8> CGameState::m_enPassant;


CGameState::CGameState()
{    
    initZobrist();
    initPieceSquareTables();
}

#ifdef ICBCOM //consistency check for debug purpose
bool CGameState::operator==(const CGameState & rGameState) const
{
    bool equal {false};
    if (m_moveGen == rGameState.m_moveGen &&
        m_history == rGameState.m_history &&
        m_pos == rGameState.m_pos)
    {
        equal = true;
    }
    return equal;
}
#endif

void CGameState::setStartPosition()
{
    for (int i = 8; i < 16; i++)
    {
        addToOccupancy(WHITE, PAWN, i);
    }
    addToOccupancy(WHITE, KNIGHT, 1);
    addToOccupancy(WHITE, KNIGHT, 6);
    addToOccupancy(WHITE, BISHOP, 2);
    addToOccupancy(WHITE, BISHOP, 5);
    addToOccupancy(WHITE, ROOK,   0);
    addToOccupancy(WHITE, ROOK,   7);
    addToOccupancy(WHITE, QUEEN,  3);
    addToOccupancy(WHITE, KING,   4);
    for (int i = 48; i < 56; i++)
    {
        addToOccupancy(BLACK, PAWN, i);
    }
    addToOccupancy(BLACK, KNIGHT, 57);
    addToOccupancy(BLACK, KNIGHT, 62);
    addToOccupancy(BLACK, BISHOP, 58);
    addToOccupancy(BLACK, BISHOP, 61);
    addToOccupancy(BLACK, ROOK,   56);
    addToOccupancy(BLACK, ROOK,   63);
    addToOccupancy(BLACK, QUEEN,  59);
    addToOccupancy(BLACK, KING,   60);
    m_moveGen.generateLegalMoves(m_moveList);
}

void CGameState::setPositionFromLayout(const std::array<SGUISquare, 64> & layout, const EColor colorToMove)
{    
    bool whiteKing {false};
    bool blackKing {false};
    for (int sq {0}; sq < 64; sq++)
    {
        const EType type {layout[sq].type};
        const EColor color {layout[sq].color};
        if (KING == type)
        {
            if (WHITE == color && !whiteKing) //force to put 1 white king!
            {
                addToOccupancy(WHITE, KING, sq);
                whiteKing = true;
            }
            if (BLACK == color && !blackKing) //force to put 1 black king!
            {
                addToOccupancy(BLACK, KING, sq);
                blackKing = true;
            }
        }
        else if (PAWN == type)
        {
            if (sq >> 3 != 0 && sq >> 3 != 7) //don't allow to put pawns on the edge ranks
            {
                addToOccupancy(color, type, sq);
            }
        }
        else if (type != NONE)
        {
            addToOccupancy(color, type, sq);
        }
    }
    if (!whiteKing) //force to put 1 white king!
    {
        addToOccupancy(WHITE, KING, 4);
    }
    if (!blackKing) //force to put 1 black king!
    {
        addToOccupancy(BLACK, KING, 60);
    }
    resetInitialCastlingRights();
    m_moveGen.setCurrentColor(colorToMove);
    m_moveGen.generateLegalMoves(m_moveList);
    handleSwitchPhase();
}

CMoveArray & CGameState::getLegalMoves()
{
    return m_moveList;
}

sint16 CGameState::getLegalMoves(CMoveArray & moveList, const int ply)
{
    sint16 score {0};
    m_moveGen.generateLegalMoves(moveList);
    if (0 == moveList.size() && m_moveGen.isCheck())
    {
        score = MIN_SCORE + ply; //checkmate
    }
    else if (!isMatingMaterial() || 100 == m_pos.fiftyCount)
    {
        moveList.clear(); //it is a draw
    }
    return score;
}

void CGameState::move(const SMove & move)
{
    m_history.pushPosition(m_pos);

    m_pos.fiftyCount++;
    if (move.capturedType != NONE) //there is a capture
    {        
        m_pos.fiftyCount = 0;
        handleSwitchPhase();
        const EColor enemy {m_moveGen.getEnemyColor()};
        delFromOccupancy(enemy, move.capturedType, move.capturedSq);
    }
    const EColor own {m_moveGen.getOwnColor()};
    delFromOccupancy(own, move.type, move.from);
    addToOccupancy(own, move.type, move.to);  //a_move.promotedType?        

    m_moveGen.setEnPassantSquare(0); //reset EPSq before next move generation
    if (PAWN == move.type)
    {
        m_pos.fiftyCount = 0;
        if (16 == std::abs(move.from - move.to)) //2-forward pawn move
        {
            //after 2Fwd set EPSq in the generator - let the moveGen add en passant capture (if a candidate exists)
            m_moveGen.setEnPassantSquare( (move.from + move.to) / 2 );
        }
        else
        {
            handlePromotion(move.to);
        }
    }
    else
    {
        handleCastling(move);
    }
    swapColors();
}

bool CGameState::move(int from, int to)
{
    bool isLegal {false};
    for (int i {0}; i < m_moveList.size(); i++)
    {
        if (m_moveList[i].from == from && m_moveList[i].to == to)
        {            
            move(m_moveList[i]);
            m_moveGen.generateLegalMoves(m_moveList);
            handleGameOver();
            isLegal = true;
            break;
        }
    }
    return isLegal;
}

void CGameState::undoMove()
{    
    if (m_history.size() > 0)
    {
        unmove();            
        uint8 EPSq { m_history.getEnPassantSquare(m_pos.occ, m_moveGen.getEnemyColor()) };
        m_moveGen.setEnPassantSquare(EPSq);
        m_moveGen.generateLegalMoves(m_moveList);
    }
}

void CGameState::choosePiece(const EType piece)
{
    const EColor enemy {m_moveGen.getEnemyColor()};
    //delFromOccupancy(enemy, PAWN, m_move.to);
    //addToOccupancy(enemy, a_piece, m_move.to);    
    m_moveGen.generateLegalMoves(m_moveList);
    //saveToHistory(PAWN); - ??
    handleGameOver();
    m_isPromotion = false;
}

uint64_array_2x6 CGameState::getOccupancy() const
{
    return m_pos.occ;
}

std::pair<uint8, uint8> CGameState::getLastMove() const
{
    return m_history.getLastMove(m_pos.occ, m_moveGen.getEnemyColor());
}

EGameResult CGameState::getGameResult() const
{
    return m_gameResult;
}

bool CGameState::isPromotion() const
{
    return m_isPromotion;
}

/*********************************************************************************************************************/
/************************************************* PRIVATE METHODS: **************************************************/
/*********************************************************************************************************************/

void CGameState::initZobrist()
{
    //fill in the zobrist table with random values
    static bool done {false};
    if (!done)
    {
        std::mt19937 rng; //random number generator
        //std::random_device rd;
        rng.seed(0xAE147AE1); //some random but constant seed (for clearer debugging)
        std::uniform_int_distribution<uint64> distribute;
        for (int sq {0}; sq < 64; sq++) // loop over the board positions
        {
            for (int type {0}; type < NONE; type++) //loop over the pieces
            {
                m_zobrist[WHITE][type][sq] = distribute(rng);
                m_zobrist[BLACK][type][sq] = distribute(rng);
            }
        }
        m_blackToMove = distribute(rng);
        m_WLCastling = distribute(rng);
        m_WSCastling = distribute(rng);
        m_BLCastling = distribute(rng);
        m_BSCastling = distribute(rng);
        //TODO: initialzie enPassant[8] hash array
        done = true; //make sure this init is done only once within the life of the application
    }
}

void CGameState::initPieceSquareTables()
{
    //add relevant pieces' values to the PST values to avoid addition in 'runtime'
    static bool done {false};
    const int piecesValues[NONE] = { 100, 310, 310, 500, 900, 0 };
    for (int type {0}; !done && type < NONE; type++)
    {
        for (int sq {0}; sq < 64; sq++)
        {
            m_PST[MIDDLE_GAME][WHITE][type][sq] += piecesValues[type];
            m_PST[MIDDLE_GAME][BLACK][type][sq] += piecesValues[type];
            m_PST[MIDDLE_GAME][BLACK][type][sq] *= -1; //actually reverse black arrays
            m_PST[END_GAME][WHITE][type][sq] += piecesValues[type];
            m_PST[END_GAME][BLACK][type][sq] += piecesValues[type];
            m_PST[END_GAME][BLACK][type][sq] *= -1; //actually reverse black arrays
        }
    }
    done = true; //make sure this init is done only once within the life of the application
}

void CGameState::resetInitialCastlingRights()
{
    if (m_pos.occ[WHITE][KING] != 0x10)
    {
        m_pos.castlRights &= 0xFD >> WHITE;
        m_pos.castlRights &= 0xF7 >> WHITE;
    }

    if (m_pos.occ[BLACK][KING] != 0x1000000000000000)
    {
        m_pos.castlRights &= 0xFD >> BLACK;
        m_pos.castlRights &= 0xF7 >> BLACK;
    }
    //add here also for rooks!!!!!!!!!!
}

void CGameState::handleSwitchPhase()
{    
    if (MIDDLE_GAME == m_pos.phase && m_moveGen.getPopCount(WHITE) + m_moveGen.getPopCount(BLACK) < 9)
    {
        m_pos.score = 0;
        for (int type {0}; type < NONE; type++) //recalculate position score
        {
            uint64 occ {m_pos.occ[WHITE][type]};
            while (occ != 0)
            {
                uint32 sq {bitScanAndReset(occ)};
                m_pos.score += m_PST[END_GAME][WHITE][type][sq];
            }
            occ = m_pos.occ[BLACK][type];
            while (occ != 0)
            {
                uint32 sq {bitScanAndReset(occ)};
                m_pos.score += m_PST[END_GAME][BLACK][type][sq];
            }
        }
        m_pos.phase = END_GAME; //switch game phase to end game
    }
}

void CGameState::addToOccupancy(const EColor color, const EType type, const int sq)
{
    m_pos.occ[color][type] |= 1ULL << sq;
    m_pos.hashKey ^= m_zobrist[color][type][sq];
    m_pos.score += m_PST[m_pos.phase][color][type][sq];
}

void CGameState::delFromOccupancy(const EColor color, const EType type, const int sq)
{
    m_pos.occ[color][type] ^= 1ULL << sq;
    m_pos.hashKey ^= m_zobrist[color][type][sq];
    m_pos.score -= m_PST[m_pos.phase][color][type][sq];
}

void CGameState::handlePromotion(const int to)
{
    if (to < 8 || to > 55)
    {
        const EColor own {m_moveGen.getOwnColor()};
        delFromOccupancy(own, PAWN, to);        
        addToOccupancy(own, QUEEN, to);
    }
}

void CGameState::handlePromotion()
{
    //if (m_move.to < 8 || m_move.to > 55)
    {
        m_isPromotion = true;
    }
}

void CGameState::handleCastling(const SMove & move) //TODO: disallow castling after rook's captured and the other takes its place
{
    if (ROOK == move.type)
    {
        const EColor own {m_moveGen.getOwnColor()};
        if (0 == (move.from & 7)) //reset the long right
        {            
            m_pos.castlRights &= 0xFD >> own; //0xFD: 1111 1101
        }
        else if (7 == (move.from & 7)) //reset the short right
        {         
            m_pos.castlRights &= 0xF7 >> own; //0xF7: 1111 0111
        }
    }
    else if (KING == move.type && 4 == (move.from & 7))
    {
        const EColor own {m_moveGen.getOwnColor()};
        if (2 == (move.to & 7)) //do long castling
        {
            delFromOccupancy(own, ROOK, move.to - 2);
            addToOccupancy(own, ROOK, move.to + 1);
        }
        else if (6 == (move.to & 7)) //do short castling
        {            
            delFromOccupancy(own, ROOK, move.to + 1);
            addToOccupancy(own, ROOK, move.to - 1);
        }
        //reset rights:
        m_pos.castlRights &= 0xFD >> own; //0xFD: 1111 1101
        m_pos.castlRights &= 0xF7 >> own; //0xF7: 1111 0111
    }
}

bool CGameState::isMatingMaterial() const
{
    if (m_moveGen.getPopCount(WHITE) > 2 || m_moveGen.getPopCount(BLACK) > 2)
    {
        return true;
    }
    else if (m_pos.occ[WHITE][PAWN] | m_pos.occ[WHITE][ROOK] | m_pos.occ[WHITE][QUEEN]
           | m_pos.occ[BLACK][PAWN] | m_pos.occ[BLACK][ROOK] | m_pos.occ[BLACK][QUEEN])
    {
        return true;
    }
    return false;
}

void CGameState::handleGameOver()
{
    if (0 == m_moveList.size())
    {
        m_gameResult = DRAW; //draw - stalemate        
        if (m_moveGen.isCheck())
        {
            m_gameResult = static_cast<EGameResult>(m_moveGen.getEnemyColor()); //wins - checkmate
        }
    }
    else if (m_history.is3FRepetition(m_pos.hashKey) || !isMatingMaterial() || 100 == m_pos.fiftyCount)
    {
        m_gameResult = DRAW;
    }
}

CGameState::sint16_array_2x2x6x64 CGameState::m_PST = {{
//Middle game:
{{
{{
{{ //pawn:
   0,   0,   0,   0,   0,   0,   0,   0,
   5,  10,  10, -20, -20,  10,  10,   5,
   5,  -5, -10,   0,   0, -10,  -5,   5,
   0,   0,   0,  20,  20,   0,   0,   0,
   5,   5,  10,  25,  25,  10,   5,   5,
  10,  10,  20,  30,  30,  20,  10,  10,
  50,  50,  50,  50,  50,  50,  50,  50,
   0,   0,   0,   0,   0,   0,   0,   0 }}, 
{{ //knight:
 -50, -40, -30, -30, -30, -30, -40, -50,
 -40, -20,   0,   5,   5,   0, -20, -40,
 -30,   5,  10,  15,  15,  10,   5, -30,
 -30,   0,  15,  20,  20,  15,   0, -30,
 -30,   5,  15,  20,  20,  15,   5, -30,
 -30,   0,  10,  15,  15,  10,   0, -30,
 -40, -20,   0,   0,   0,   0, -20, -40,
 -50, -40, -30, -30, -30, -30, -40, -50 }}, 
{{ //bishop:
 -20, -10, -10, -10, -10, -10, -10, -20,
 -10,   5,   0,   0,   0,   0,   5, -10,
 -10,  10,  10,  10,  10,  10,  10, -10,
 -10,   0,  10,  10,  10,  10,   0, -10,
 -10,   5,   5,  10,  10,   5,   5, -10,
 -10,   0,   5,  10,  10,   5,   0, -10,
 -10,   0,   0,   0,   0,   0,   0, -10,
 -20, -10, -10, -10, -10, -10, -10, -20 }}, 
{{ //rook:
   0,   0,   0,   5,   5,   0,   0,   0,
  -5,   0,   0,   0,   0,   0,   0,  -5,
  -5,   0,   0,   0,   0,   0,   0,  -5,
  -5,   0,   0,   0,   0,   0,   0,  -5,
  -5,   0,   0,   0,   0,   0,   0,  -5,
  -5,   0,   0,   0,   0,   0,   0,  -5,
   5,  10,  10,  10,  10,  10,  10,   5,
   0,   0,   0,   0,   0,   0,   0,   0 }}, 
{{ //queen:
 -20, -10, -10,  -5,  -5, -10, -10, -20,
 -10,   0,   5,   0,   0,   0,   0, -10,
 -10,   5,   5,   5,   5,   5,   0, -10,
   0,   0,   5,   5,   5,   5,   0,  -5,
  -5,   0,   5,   5,   5,   5,   0,  -5,
 -10,   0,   5,   5,   5,   5,   0, -10,
 -10,   0,   0,   0,   0,   0,   0, -10,
 -20, -10, -10,  -5,  -5, -10, -10, -20 }},
{{ //king:
  20,  30,  10,   0,   0,  10,  30,  20,
  20,  20,   0,   0,   0,   0,  20,  20,
 -10, -20, -20, -20, -20, -20, -20, -10,
 -20, -30, -30, -40, -40, -30, -30, -20,
 -30, -40, -40, -50, -50, -40, -40, -30,
 -30, -40, -40, -50, -50, -40, -40, -30,
 -30, -40, -40, -50, -50, -40, -40, -30,
 -30, -40, -40, -50, -50, -40, -40, -30 }}
}},
{{
{{ //pawn:
   0,   0,   0,   0,   0,   0,   0,   0,
  50,  50,  50,  50,  50,  50,  50,  50,
  10,  10,  20,  30,  30,  20,  10,  10,
   5,   5,  10,  25,  25,  10,   5,   5,
   0,   0,   0,  20,  20,   0,   0,   0,
   5,  -5, -10,   0,   0, -10,  -5,   5,
   5,  10,  10, -20, -20,  10,  10,   5,
   0,   0,   0,   0,   0,   0,   0,   0 }},
{{ //knight:
 -50, -40, -30, -30, -30, -30, -40, -50,
 -40, -20,   0,   0,   0,   0, -20, -40,
 -30,   0,  10,  15,  15,  10,   0, -30,
 -30,   5,  15,  20,  20,  15,   5, -30,
 -30,   0,  15,  20,  20,  15,   0, -30,
 -30,   5,  10,  15,  15,  10,   5, -30,
 -40, -20,   0,   5,   5,   0, -20, -40,
 -50, -40, -30, -30, -30, -30, -40, -50 }},
{{ //bishop:
 -20, -10, -10, -10, -10, -10, -10, -20,
 -10,   0,   0,   0,   0,   0,   0, -10,
 -10,   0,   5,  10,  10,   5,   0, -10,
 -10,   5,   5,  10,  10,   5,   5, -10,
 -10,   0,  10,  10,  10,  10,   0, -10,
 -10,  10,  10,  10,  10,  10,  10, -10,
 -10,   5,   0,   0,   0,   0,   5, -10,
 -20, -10, -10, -10, -10, -10, -10, -20 }},
{{ //rook:
   0,   0,   0,   0,   0,   0,   0,   0,
   5,  10,  10,  10,  10,  10,  10,   5,
  -5,   0,   0,   0,   0,   0,   0,  -5,
  -5,   0,   0,   0,   0,   0,   0,  -5,
  -5,   0,   0,   0,   0,   0,   0,  -5,
  -5,   0,   0,   0,   0,   0,   0,  -5,
  -5,   0,   0,   0,   0,   0,   0,  -5,
   0,   0,   0,   5,   5,   0,   0,   0 }},
{{ //queen:
 -20, -10, -10,  -5,  -5, -10, -10, -20,
 -10,   0,   0,   0,   0,   0,   0, -10,
 -10,   0,   5,   5,   5,   5,   0, -10,
  -5,   0,   5,   5,   5,   5,   0,  -5,
   0,   0,   5,   5,   5,   5,   0,  -5,
 -10,   5,   5,   5,   5,   5,   0, -10,
 -10,   0,   5,   0,   0,   0,   0, -10,
 -20, -10, -10,  -5,  -5, -10, -10, -20 }},
{{ //king:
 -30, -40, -40, -50, -50, -40, -40, -30,
 -30, -40, -40, -50, -50, -40, -40, -30,
 -30, -40, -40, -50, -50, -40, -40, -30,
 -30, -40, -40, -50, -50, -40, -40, -30,
 -20, -30, -30, -40, -40, -30, -30, -20,
 -10, -20, -20, -20, -20, -20, -20, -10,
  20,  20,   0,   0,   0,   0,  20,  20,
  20,  30,  10,   0,   0,  10,  30,  20 }}
}}
}},
//End game:
{{
{{
{{ //pawn:
   0,   0,   0,   0,   0,   0,   0,   0,
  10,  10,  10,  10,  10,  10,  10,  10,
  25,  25,  30,  30,  30,  30,  25,  25,
  40,  40,  40,  50,  50,  40,  40,  40,
  50,  55,  60,  70,  70,  60,  55,  50,
  75,  80,  80,  80,  80,  80,  80,  75,
  95, 100, 100, 100, 100, 100, 100,  95,
  0,   0,   0,   0,   0,   0,   0,   0  }},
{{ //knight:
 -50, -40, -30, -30, -30, -30, -40, -50,
 -40, -20,   0,   5,   5,   0, -20, -40,
 -30,   0,  10,  15,  15,  10,   0, -30,
 -30,   5,  15,  20,  20,  15,   5, -30,
 -30,   5,  15,  20,  20,  15,   5, -30,
 -30,   0,  10,  15,  15,  10,   0, -30,
 -40, -20,   0,   0,   0,   0, -20, -40,
 -50, -40, -30, -30, -30, -30, -40, -50 }}, 
{{ //bishop:
 -20, -10, -10, -10, -10, -10, -10, -20,
 -10,   0,   0,   0,   0,   0,   0, -10,
 -10,   0,  10,  10,  10,  10,   0, -10,
 -10,   5,  10,  15,  15,  10,   5, -10,
 -10,   5,  10,  15,  15,  10,   5, -10,
 -10,   0,  10,  10,  10,  10,   0, -10,
 -10,   0,   0,   0,   0,   0,   0, -10,
 -20, -10, -10, -10, -10, -10, -10, -20 }}, 
{{ //rook:
   0,   0,   0,   0,   0,   0,   0,   0,
   5,   0,   0,   0,   0,   0,   0,   5,
  -5,   0,   0,   0,   0,   0,   0,  -5,
  -5,   0,   0,   0,   0,   0,   0,  -5,
  -5,   0,   0,   0,   0,   0,   0,  -5,
  -5,   0,   0,   0,   0,   0,   0,  -5,
   5,   0,   0,   0,   0,   0,   0,   5,
   0,   0,   0,   0,   0,   0,   0,   0 }}, 
{{ //queen:
 -20, -10, -10,  -5,  -5, -10, -10, -20,
 -10,   0,   5,   0,   0,   0,   0, -10,
 -10,   5,   5,   5,   5,   5,   0, -10,
   0,   0,   5,   5,   5,   5,   0,   0,
   0,   0,   5,   5,   5,   5,   0,   0,
 -10,   0,   5,   5,   5,   5,   0, -10,
 -10,   0,   0,   0,   0,   0,   0, -10,
 -20, -10, -10,  -5,  -5, -10, -10, -20 }},
{{ //king:
 -50, -30, -30, -30, -30, -30, -30, -50,
 -30, -30,   0,   0,   0,   0, -30, -30,
 -30, -10,  20,  30,  30,  20, -10, -30,
 -30, -10,  30,  40,  40,  30, -10, -30,
 -30, -10,  30,  40,  40,  30, -10, -30,
 -30, -10,  20,  30,  30,  20, -10, -30,
 -30, -20, -10,   0,   0, -10, -20, -30,
 -50, -40, -30, -20, -20, -30, -40, -50 }},
}},
{{
{{ //pawn:
   0,   0,   0,   0,   0,   0,   0,   0,
  95, 100, 100, 100, 100, 100, 100,  95,
  75,  80,  80,  80,  80,  80,  80,  75,
  50,  55,  60,  70,  70,  60,  55,  50,
  40,  40,  40,  50,  50,  40,  40,  40,
  25,  25,  30,  30,  30,  30,  25,  25,
  10,  10,  10,  10,  10,  10,  10,  10,
   0,   0,   0,   0,   0,   0,   0,   0 }},
{{ //knight:
 -50, -40, -30, -30, -30, -30, -40, -50,
 -40, -20,   0,   0,   0,   0, -20, -40,
 -30,   0,  10,  15,  15,  10,   0, -30,
 -30,   5,  15,  20,  20,  15,   5, -30,
 -30,   5,  15,  20,  20,  15,   5, -30,
 -30,   0,  10,  15,  15,  10,   0, -30,
 -40, -20,   0,   5,   5,   0, -20, -40,
 -50, -40, -30, -30, -30, -30, -40, -50 }},
{{ //bishop:
 -20, -10, -10, -10, -10, -10, -10, -20,
 -10,   0,   0,   0,   0,   0,   0, -10,
 -10,   0,  10,  10,  10,  10,   0, -10,
 -10,   5,  10,  15,  15,  10,   5, -10,
 -10,   5,  10,  15,  15,  10,   5, -10,
 -10,   0,  10,  10,  10,  10,   0, -10,
 -10,   0,   0,   0,   0,   0,   0, -10,
 -20, -10, -10, -10, -10, -10, -10, -20 }},
{{ //rook:
   0,   0,   0,   0,   0,   0,   0,   0,
   5,   0,   0,   0,   0,   0,   0,   5,
  -5,   0,   0,   0,   0,   0,   0,  -5,
  -5,   0,   0,   0,   0,   0,   0,  -5,
  -5,   0,   0,   0,   0,   0,   0,  -5,
  -5,   0,   0,   0,   0,   0,   0,  -5,
   5,   0,   0,   0,   0,   0,   0,   5,
   0,   0,   0,   0,   0,   0,   0,   0 }},
{{ //queen:
 -20, -10, -10,  -5,  -5, -10, -10, -20,
 -10,   0,   0,   0,   0,   0,   0, -10,
 -10,   0,   5,   5,   5,   5,   0, -10,
   0,   0,   5,   5,   5,   5,   0,   0,
   0,   0,   5,   5,   5,   5,   0,   0,
 -10,   5,   5,   5,   5,   5,   0, -10,
 -10,   0,   5,   0,   0,   0,   0, -10,
 -20, -10, -10,  -5,  -5, -10, -10, -20 }},
{{ //king:
 -50, -40, -30, -20, -20, -30, -40, -50,
 -30, -20, -10,   0,   0, -10, -20, -30,
 -30, -10,  20,  30,  30,  20, -10, -30,
 -30, -10,  30,  40,  40,  30, -10, -30,
 -30, -10,  30,  40,  40,  30, -10, -30,
 -30, -10,  20,  30,  30,  20, -10, -30,
 -30, -30,   0,   0,   0,   0, -30, -30,
 -50, -30, -30, -30, -30, -30, -30, -50 }}
}}
}}
}};

}//namespace fool




