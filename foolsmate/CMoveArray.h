/*Welcome!
  This is a simple and purely educational chess engine named "foolsmate".
  For best move search it uses the negamax algorithm with alpha-beta pruning + transposition table,
  iterative deepening, simple move ordering (hashmove, captures, killermove and history moves),
  and basic position score evaluation (material + PST).
  Move generation is bitboard-based with precalculated attack sets and magic numbers (for xray pieces).
  The only chess-specific source of knowledge: www.chessprogramming.org;

  In the code (identifiers/comments) you may find a few abbreviations/acronyms listed below:
     sq - square (on a chessboard), usually uint8 with a value <0, 63>
    occ - occupancy: the exact arrangement of chess pieces on the chessboard in a given moment;
          always expressed by a bitboard (uint64) or a set of bitboards, e.g. all pieces, only one type, only one color, etc
    pos - (game) position
    set - usually an attack set (or move set) as a bitboard, possibly other 'bitboard subset'
   1Fwd - normal pawn movement (a push) by 1 square forward
   2Fwd - a push by 2 squares forward (possible only from the initial position)
   EPSq - en passant square (the one on which the attaking pawn rests after the capture)
2FR/3FR - derived from Three-Fold-Repetition rule
          which causes draw after 3 identical positions (or just 2 for the sake of AI search)
 AI/CAI - (class) artificial intelligence - best move search component / CAISearchAgent object / 'AI search'
     TT - transposition table    
    LUT - look up table (CLookupData object, which serves to moveGen)
    PST - piece square tables (used for score evaluation)    
*/

#ifndef FOOLSMATE_CMOVEARRAY
#define FOOLSMATE_CMOVEARRAY

#include <array>
#include <string>
#include "CommonStuff.h"

namespace fool
{

struct SMove
{    
    //grade of a move used for ordering, it is assigned to all captures (static heuristic),
    //to hashmove, killermovea and history moves (dynamic heuristics);
    uint32 grade {0};
    
    //index of a source square, <0-63> are valid values
    uint8 from {255};

    //index of a destination square, <0-63> only valid values
    uint8 to {255};
    
    //type of piece to be moved
    EType type {NONE};

    //type of piece to be captured by this move ('NONE' if there's no capture)
    EType capturedType {NONE};
    //EType promotedType; todo(?)

    //square index of captured piece (introduced for the sake of en passant capture)
    uint8 capturedSq {NONE};    
};

class CMoveArray
{
    public:
    //************************ PUBLIC METHODS: **************************
    
    const SMove & operator[](int index) const;

    //returns a number of all legal moves which are added to the move array by move generator after last move;
    int size() const;

    //resets the m_size and m_gradedSize counters (but not memory - it'd be too expensive anyway);
    void clear();

    //inserts a non-capture to the move array, called by the move generator;
    void insertMove(uint8 from, uint8 to, EType type);
    
    //inserts a capture to the move array and assigns a grade, called by the move generator;
    void insertCapture(uint8 from, uint8 to, EType type, EType capturedType);

    //inserts an EP capture to the move array and assigns a grade, called by the move generator;
    void insertEnPassant(uint8 from, uint8 to);
    
    //simpler overload which considers only a hashMove from the TT, used only in a root node of AI search;
    void orderMoves(const SMove & hashmove);
    
    //full-feature overload which takes all dynamic heuristics into account (hashMove, killerMove and history moves);    
    void orderMoves(const SMove & hashmove, const SMove & killerMove, const uint32_array_64x64 & historyMoves, int depth);

    //converts move coordinates (from/to) to chessboard format (eg. 'c3d4');
    //used only for printing in PERFT/DIVIDE testing procedure;
    std::string toString(int index) const;


    private:
    //************************ PRIVATE METHODS: **************************

    //swaps two elements' indices;
    void swap(const int idx1, const int idx2);

    //finds index of hashmove (if provided by AI search), assigns a corresponding grade and shifts to graded moves;
    void findHashMoveAndGrade(const SMove & hashMove);

    //finds index of killermove (if provided by AI search), assigns a corresponding grade and shifts to graded moves;
    void findKillerAndGrade(const SMove & killerMove);

    //sorts moves by their grades up to m_gradedSize index only (captures + hashmove + killermove + history moves);
    void selectionSort();    


    //************************* DATA MEMBERS: ***************************

    static const int CAPACITY {128};
    static const uint32 HASHMOVE_GRADE   {20 + 1000000};  //+1000000 to surpass history grades which must be lower than hashmove
    static const uint32 KILLERMOVE_GRADE {10 + 1000000};  //...and lower than killer move
        
    //actual move array
    std::array<SMove, CAPACITY> m_array;

    uint8 m_size {0};

    //graded moves counter; on behalf of move ordering speed each move which is graded (in insert..() and in orderMoves())
    //is also shifted to left-most part and this counter is incremented; thanks to that selectionSort() sorts only
    //graded moves; all moves with index greater than m_gradedSize are treated like grade 0 and sorting them is pointless;
    uint8 m_gradedSize {0};
};

inline const SMove & CMoveArray::operator[](int index) const
{
    return m_array[index];
}

inline int CMoveArray::size() const
{
    return m_size;
}

inline void CMoveArray::clear()
{
    m_size = 0;
    m_gradedSize = 0;
}

}//namespace fool

#endif //FOOLSMATE_CMOVEARRAY

