#include <iostream> //for logging
#include "CAISearchAgent.h"

namespace fool
{

CAISearchAgent::CAISearchAgent()
{    
    resetData();
}

void CAISearchAgent::abortSearch()
{
    m_searchAborted = true;    
    m_TTable.clear();
}

bool CAISearchAgent::isSearchAborted() const
{
    return m_searchAborted;
}

void CAISearchAgent::resetData()
{
    m_leafNodesCount = 0;
    m_searchAborted = false;
    std::fill(m_killerMove.begin(), m_killerMove.end(), SMove());
    std::memset(m_historyMoves.data(), 0, sizeof(uint32) * COLORS * 64 * 64);
    m_moveList = std::make_unique<CMoveArray[]>(m_targetDepth);    
}

void CAISearchAgent::handleTargetDepthIncrease(sint16 bestScore)
{
    if (m_currDepth == m_targetDepth && bestScore < MAX_SCORE - MAX_SEARCH_DEPTH &&
        m_leafNodesCount < DEPTH_INCR_TREE_SIZE_THRESHOLD && m_currDepth < MAX_SEARCH_DEPTH)
    {
        m_targetDepth++;        
        m_moveList = std::make_unique<CMoveArray[]>(m_targetDepth);
        m_TTable.clear();
        std::cout << "m_currDepth: " << m_currDepth << ";  (depth incr) leaves: " << m_leafNodesCount << std::endl;
    }
}

#ifndef PERFT
SMove CAISearchAgent::runNegamax(int searchDepth, CGameState & gameState)
{
    std::cout << "\n\n**********************\n\n";
    m_targetDepth = searchDepth;    
    m_gameState = &gameState;
    resetData();
    SMove bestMove;
    CMoveArray & moves = m_gameState->getLegalMoves();
    for (m_currDepth = 1; !m_searchAborted && m_currDepth <= m_targetDepth; m_currDepth++)
    {
        SMove hashMove;             
        if ( m_TTable.isHashKeyValid(m_gameState->getHashKey()) )
        {
            hashMove = m_TTable.getHashMove();
        }          
        moves.orderMoves(hashMove);        
        sint16 bestScore {MIN_SCORE};
        for (int i = 0; i < moves.size() && !m_searchAborted; i++)
        {                        
            m_gameState->move(moves[i]);
            sint16 score {-negamax(m_currDepth - 1, MIN_SCORE, -bestScore)};
            m_gameState->unmove();
            if (score > bestScore)
            {
                bestScore = score;
                bestMove = moves[i];                
            }            
        }
        m_TTable.insertEntry(m_gameState->getHashKey(), bestScore, EXACT, m_currDepth, bestMove);
        handleTargetDepthIncrease(bestScore);
    }
    std::cout << "depth reached: " << m_targetDepth << ";  visited leaves: " << m_leafNodesCount << std::endl;
    return bestMove;
}
#endif

sint16 CAISearchAgent::negamax(const int depth, int alpha, const int beta)
{
    const int ply {m_currDepth - depth};
    if (0 == depth)
    {
        m_leafNodesCount++;
        return m_gameState->getPositionScore(ply);
    }
    if (m_gameState->isPositionRepeated()) return 0; //treat repetition as a draw

    SMove hashMove;
    if ( m_TTable.isHashKeyValid(m_gameState->getHashKey()) )
    {
        if (m_TTable.isScoreApplicable(depth, alpha, beta))
        {
            return m_TTable.getScore();
        }
        hashMove = m_TTable.getHashMove();
    }
        
    CMoveArray & moves {m_moveList[depth]};
    sint16 endScore {m_gameState->getLegalMoves(moves, ply)};
    if (0 == moves.size()) return endScore;
    
    moves.orderMoves(hashMove, m_killerMove[ply], m_historyMoves[ply & 0x1], depth);
    SMove bestMove;
    sint16 bestScore {MIN_SCORE};
    for (int i = 0; i < moves.size() && !m_searchAborted; i++)
    {
#ifdef ICBCOM
        CGameState debugCopy = *m_gameState;
#endif
        m_gameState->move(moves[i]);
        sint16 score {-negamax(depth - 1, -beta, -alpha)};
        m_gameState->unmove();
#ifdef ICBCOM
        if (!(debugCopy == *m_gameState)) throw ("Inconsistency detection!!");
#endif
        if (score >= beta)
        {
            m_TTable.insertEntry(m_gameState->getHashKey(), score, LOWERBOUND, depth, moves[i]);
            if (NONE == moves[i].capturedType)
            {
                m_killerMove[ply] = moves[i];
                m_historyMoves[ply & 0x1][ moves[i].from ][ moves[i].to ] += depth*depth;
            }            
            return score;
        }
        if (score > bestScore)
        {
            bestScore = score;
            if (score > alpha)
            {
                alpha = score;
                bestMove = moves[i];
            }
        }        
    }
    const EScoreType scoreType {bestMove.from != 255 ? EXACT : UPPERBOUND};
    m_TTable.insertEntry(m_gameState->getHashKey(), bestScore, scoreType, depth, bestMove);
    return bestScore;
}

#ifdef PERFT
//stockfish: position fen rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1
//stockfish: d (show chessboard)
//stockfish: perft 5
int g_divide[256];
int g_rootChildIdx;
SMove CAISearchAgent::runNegamax(int searchDepth, CGameState & gameState)
{
    m_gameState = &gameState;
    m_searchAborted = false;   std::cout << "\n\n******** PERFT *******\n\n";
    m_targetDepth = searchDepth;
    m_leafNodesCount = 0;
    SMove move;
    for (int i = 0; i < 256; i++)
    {
        g_divide[i] = 0;
    }
    g_rootChildIdx = 0;
    uint64 nodes {0};
    CMoveArray & moves {m_gameState->getLegalMoves()};
    for (int i = 0; i < moves.size(); i++)
    {        
        g_rootChildIdx = i;        
        m_gameState->move(moves[i]);
        nodes += perft(a_searchDepth - 1);
        m_gameState->unmove();
        move = moves[i]; //<- this is needed to get time measure in C#
        std::cout << moves.toString(i) << ": " << g_divide[i] << std::endl;
    }
    std::cout << " visited gs: " << m_leafNodesCount << std::endl;
    std::cout << "perft nodes: " << nodes << std::endl;
    return move;
}

uint64 CAISearchAgent::perft(int depth)
{
    if (m_searchAborted) return 0;

    if (0 == depth)
    {
        m_leafNodesCount++; g_divide[g_rootChildIdx]++;
        return 1;
    }        
    CMoveArray & moves {m_moveList[depth]};
    sint16 endScore {m_gameState->getLegalMoves(moves, m_targetDepth - depth)};
    uint64 nodes = 0;
    for (int i = 0; i < moves.size(); i++)
    {        
        m_gameState->move(moves[i]);
        nodes += perft(depth - 1);
        m_gameState->unmove();
    }
    return nodes;
}
#endif

}//namespace fool