#include <iostream>
#include "CMoveArray.h"

namespace fool
{

void CMoveArray::insertMove(const uint8 from, const uint8 to, const EType type)
{
    m_array[m_size].from = from;
    m_array[m_size].to = to;
    m_array[m_size].type = type;
    m_array[m_size].capturedType = NONE;    
    m_size++;
}

void CMoveArray::insertCapture(const uint8 from, const uint8 to, const EType type, const EType capturedType)
{
    m_array[m_size] = m_array[m_gradedSize];
    m_array[m_gradedSize].from = from;
    m_array[m_gradedSize].to = to;
    m_array[m_gradedSize].type = type;
    m_array[m_gradedSize].capturedType = capturedType;
    m_array[m_gradedSize].capturedSq = to;
    //good captures are graded over killer moves, bad ones under (but only a direct exchange decides for now):
    m_array[m_gradedSize].grade = KILLERMOVE_GRADE + capturedType - type;
    m_gradedSize++;
    m_size++;
}

void CMoveArray::insertEnPassant(const uint8 from, const uint8 to)
{
    m_array[m_size] = m_array[m_gradedSize];
    m_array[m_gradedSize].from = from;
    m_array[m_gradedSize].to = to;
    m_array[m_gradedSize].type = PAWN;
    m_array[m_gradedSize].capturedType = PAWN;
    m_array[m_gradedSize].capturedSq = (to & 7) + (from >> 3) * 8;
    //good captures are graded over killer moves, bad ones under (but only a direct exchange decides for now):
    m_array[m_gradedSize].grade = KILLERMOVE_GRADE; //+ PAWN - PAWN
    m_gradedSize++;
    m_size++;
}

void CMoveArray::orderMoves(const SMove & hashMove)
{
    findHashMoveAndGrade(hashMove);
    selectionSort();
}

void CMoveArray::orderMoves(const SMove & hashMove, const SMove & killerMove,
                            const uint32_array_64x64 & historyMoves, const int depth)
{
    findHashMoveAndGrade(hashMove);
    findKillerAndGrade(killerMove);

    if (depth > 1) //don't sort history moves at depth 1 (too expensive)
    {
        for (int i {m_gradedSize}; i < m_size; i++)
        {
            uint32 historyGrade {historyMoves[m_array[i].from][m_array[i].to]};
            if (historyGrade > 0)
            {
                m_array[i].grade = historyGrade;
                swap(i, m_gradedSize); //shift to the left-most subset of 'graded' moves to speed up selection sort
                m_gradedSize++;
            }
        }
    }

    selectionSort();
}

#ifdef PERFT
std::string CMoveArray::toString(const int idx) const
{
    std::string str = {static_cast<char>( (m_array[idx].from & 7) + 97 ),
                       static_cast<char>( (m_array[idx].from >> 3) +1 +48 ),
                       static_cast<char>( (m_array[idx].to & 7) + 97 ),
                       static_cast<char>( (m_array[idx].to >> 3) +1 +48 )};
    str += " (" + std::to_string(m_array[idx].from) + ", "
                + std::to_string(m_array[idx].to) + ")";
    return str;
}
#endif

/*********************************************************************************************************************/
/************************************************* PRIVATE METHODS: **************************************************/
/*********************************************************************************************************************/

void CMoveArray::swap(const int idx1, const int idx2)
{
    if (idx1 != idx2)
    {
        const SMove temp = m_array[idx1];
        m_array[idx1] = m_array[idx2];
        m_array[idx2] = temp; 
    }
}

void CMoveArray::findHashMoveAndGrade(const SMove & hashMove)
{
    if (hashMove.from != 255)
    {
        for (int i {0}; i < m_size; i++) //find the index of the hashmove in the array
        {
            if (hashMove.from == m_array[i].from && hashMove.to == m_array[i].to)
            {
                m_array[i].grade = HASHMOVE_GRADE;
                if (i >= m_gradedSize)
                {
                    swap(i, m_gradedSize); //shift to the left-most subset of 'graded' moves to speed up selection sort
                    m_gradedSize++;
                }
                break;
            }
        }
    }
}

void CMoveArray::findKillerAndGrade(const SMove & killerMove)
{
    if (killerMove.from != 255)
    {
        for (int i {m_gradedSize}; i < m_size; i++) //find the index of the killer move in the array
        {
            if (killerMove.from == m_array[i].from && killerMove.to == m_array[i].to)
            {
                m_array[i].grade = KILLERMOVE_GRADE;
                swap(i, m_gradedSize); //shift to the left-most subset of 'graded' moves to speed up selection sort
                m_gradedSize++;
                break;
            }
        }
    }
}

void CMoveArray::selectionSort()
{    
    for (int i {0}; i < m_gradedSize - 1; i++)
    {
        int max {i};
        for (int j {i + 1}; j < m_gradedSize; j++)
        {
            if (m_array[j].grade > m_array[max].grade)
            {
                max = j;
            }
        }
        swap(i, max);
    }
}

}//namespace fool
