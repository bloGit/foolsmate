/*Welcome!
  This is a simple and purely educational chess engine named "foolsmate".
  For best move search it uses the negamax algorithm with alpha-beta pruning + transposition table,
  iterative deepening, simple move ordering (hashmove, captures, killermove and history moves),
  and basic position score evaluation (material + PST).
  Move generation is bitboard-based with precalculated attack sets and magic numbers (for xray pieces).
  The only chess-specific source of knowledge: www.chessprogramming.org;

  In the code (identifiers/comments) you may find a few abbreviations/acronyms listed below:
     sq - square (on a chessboard), usually uint8 with a value <0, 63>
    occ - occupancy: the exact arrangement of chess pieces on the chessboard in a given moment;
          always expressed by a bitboard (uint64) or a set of bitboards, e.g. all pieces, only one type, only one color, etc
    pos - (game) position
    set - usually an attack set (or move set) as a bitboard, possibly other 'bitboard subset'
   1Fwd - normal pawn movement (a push) by 1 square forward
   2Fwd - a push by 2 squares forward (possible only from the initial position)
   EPSq - en passant square (the one on which the attaking pawn rests after the capture)
2FR/3FR - derived from Three-Fold-Repetition rule
          which causes draw after 3 identical positions (or just 2 for the sake of AI search)
 AI/CAI - (class) artificial intelligence - best move search component / CAISearchAgent object / 'AI search'
     TT - transposition table    
    LUT - look up table
    PST - piece square tables (used for score evaluation)    
*/
  
#ifndef FOOLSMATE_COMMONSTUFF
#define FOOLSMATE_COMMONSTUFF

#include <intrin.h> //_BitScanForward64, _BitScanReverse64
#include <limits> //std::numeric_limits

//#define PERFT
//#define ICBCOM //Inconsistency Check By COMparison

namespace fool
{

//*********** COMMON TYPES: ************

using uint8  = unsigned char;
using uint16 = unsigned short;
using uint32 = unsigned long;
using uint64 = unsigned long long;
using sint16 = signed short;

enum EType: uint8
{
    PAWN   = 0,
    KNIGHT = 1,
    BISHOP = 2,
    ROOK   = 3,
    QUEEN  = 4,
    KING   = 5,
    NONE   = 6
};

enum EColor: uint8
{
    WHITE  = 0,
    BLACK  = 1,
    COLORS = 2
};

enum EGameResult: uint8 //enum used to tell GUI about the game result
{
    WHITE_WINS  = 0,
    BLACK_WINS  = 1,
    DRAW        = 2,
    IN_PROGRESS = 3
}; 

using uint64_array_2x6   = std::array< std::array<uint64, NONE>, COLORS>;
using uint32_array_64x64 = std::array< std::array<uint32, 64>, 64>;

//struct represents a single square, used as an element of 64-sized array (a chessboard)
//for the purpose of communication with GUI (m_redraw callback and setCustomPosition)
struct SGUISquare
{
    uint64 destSquares {0}; //bitboard containing destination squares (legal moves) available from this square
    EType type {NONE};
    EColor color {WHITE};
    bool lastMove {false};
};


//************************ GLOBAL CONSTANTS: **************************

const uint8 MAX_SEARCH_DEPTH = 26;

const sint16 MIN_SCORE = std::numeric_limits<sint16>::min() + 1;
const sint16 MAX_SCORE = std::numeric_limits<sint16>::max();


//************************ GLOBAL FUNCTIONS: **************************

//obtains a bit index <0-63> of least significant bit '1' (LSB1);
inline uint8 bitScan(uint64 bitboard)
{
    uint32 sq;
    _BitScanForward64(&sq, bitboard);
    return static_cast<uint8>(sq);
}


//obtains a bit index <0-63> of least significant bit '1' and resets it (to prepare for next scan - intended for loops);
inline uint8 bitScanAndReset(uint64 & bitboard)
{
    uint32 sq;
    _BitScanForward64(&sq, bitboard);
    bitboard ^= 1ULL << sq; //reset scanned bit, next time this bitboard is passed here a next bit will be scanned
    return static_cast<uint8>(sq);
}


//'symmetric' overload in terms of scanning WHITE or BLACK pieces, in seems to perform better than unidirectional scan;
inline uint8 bitScanAndReset(uint64 & bitboard, EColor color)
{
    uint32 sq;
    if (WHITE == color)
    {
        _BitScanReverse64(&sq, bitboard);
    }
    else
    {
        _BitScanForward64(&sq, bitboard);
    }
    bitboard ^= 1ULL << sq; //reset scanned bit, next time this bitboard is passed here a next bit will be scanned
    return static_cast<uint8>(sq);
}

}//namespace fool

#endif //FOOLSMATE_COMMONSTUFF