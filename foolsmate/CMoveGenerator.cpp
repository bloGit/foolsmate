#include "CMoveGenerator.h"
#include "CGameHistory.h" //SPosition
#include <iostream>

namespace fool
{

const CLookupData CMoveGenerator::m_LUTData;

const uint64 CMoveGenerator::RANK_2Fwd[2] = { 0x000000000000FF00, 0x00FF000000000000 };


CMoveGenerator::CMoveGenerator(const uint64_array_2x6 & occ, const uint8 & cRights): m_occ(occ), m_castlRights(cRights)
{
    m_fullOcc[m_own] = m_fullOcc[m_enemy] = 0;
    std::cout << "sizeof(m_LUTData): " << sizeof(m_LUTData) << std::endl;
}

#ifdef ICBCOM //consistency check for debug purpose
bool CMoveGenerator::operator==(const CMoveGenerator & rGameState) const
{   
    return m_own == rGameState.m_own && m_enemy == rGameState.m_enemy;
}
#endif

void CMoveGenerator::setCurrentColor(EColor color)
{
    m_own = color;
    m_enemy = (WHITE == m_own ? BLACK : WHITE);    
}

void CMoveGenerator::generateLegalMoves(CMoveArray & moveList)
{
    m_pMoveList = &moveList;
    m_pMoveList->clear();
    const uint64 checker {determineCheck()};
    updateEnemyAttackSet();
    if (0 == checker) //no check
    {
        preventHorizontallyPinnedEP();
        uint64 done {m_occ[m_own][KING]};
        updatePinnedPieces(done);
        updateRemainingPieces(done);
        updateCastling();
    }
    else if (0 == (checker & checker - 1)) //there is a single check
    {
        updateCheckDefence(checker);
    }    
    updateKing(); //update king at the end (best for move ordering)
    //if double-check nothing more to do - either the king can move or no-one can (checkmate)
}

bool CMoveGenerator::isCheckmate()
{    
    const uint64 checker {determineCheck()};
    if (0 == checker)
    {
        return false; //no check - no chackmate
    }
    updateEnemyAttackSet();
    if (m_LUTData.getKingAttackSet(m_king, m_fullOcc[m_own]) & ~m_fullOcc[m_own] & ~m_enemyAttackSet)
    {
        return false; //the king can move - no checkmate
    }
    if (0 == (checker & checker - 1)) //single check
    {
        return !isCheckDefencePossible(checker);
    }
    return true; //double check and king can't move - checkmate
}

/*********************************************************************************************************************/
/************************************************* PRIVATE METHODS: **************************************************/
/*********************************************************************************************************************/

EType CMoveGenerator::getType(const int sq) const
{
    for (int type {0}; type < KING; type++)
    {
        if (1ULL << sq & m_occ[m_own][type])
        {
            return static_cast<EType>(type);
        }
    }
    throw ("INVALID PIECE TYPE");
}

EType CMoveGenerator::getEnemyType(const int sq) const
{
    for (int type {0}; type < KING; type++)
    {
        if (1ULL << sq & m_occ[m_enemy][type])
        {
            return static_cast<EType>(type);
        }
    }
    throw ("INVALID PIECE TYPE");
}

uint64 CMoveGenerator::determineCheck()
{    
    m_fullOcc[WHITE] = m_occ[WHITE][PAWN] | m_occ[WHITE][KNIGHT] | m_occ[WHITE][BISHOP]
                     | m_occ[WHITE][ROOK] | m_occ[WHITE][QUEEN]  | m_occ[WHITE][KING];
    m_fullOcc[BLACK] = m_occ[BLACK][PAWN] | m_occ[BLACK][KNIGHT] | m_occ[BLACK][BISHOP]
                     | m_occ[BLACK][ROOK] | m_occ[BLACK][QUEEN]  | m_occ[BLACK][KING];
    m_all = m_fullOcc[WHITE] | m_fullOcc[BLACK];
    m_king = bitScan(m_occ[m_own][KING]);

    return m_LUTData.getPawnMask(m_king, m_own) & m_occ[m_enemy][PAWN]
         | m_LUTData.getKnightAttackSet(m_king, m_fullOcc[m_own]) & m_occ[m_enemy][KNIGHT]
         | m_LUTData.getBishopAttackSet(m_king, m_all)
             & (m_occ[m_enemy][BISHOP] | m_occ[m_enemy][QUEEN])
         | m_LUTData.getRookAttackSet(m_king, m_all)
             & (m_occ[m_enemy][ROOK] | m_occ[m_enemy][QUEEN]);
}

void CMoveGenerator::updateEnemyAttackSet()
{
    //update pawns setwise    
    const uint64 pawns {m_occ[m_enemy][PAWN]};
    const int shift = {8 - (m_own << 4)};
    m_enemyAttackSet = _rotr64(pawns, shift - 1) & NOT_A_FILE | _rotr64(pawns, shift + 1) & NOT_H_FILE;

    m_all ^= m_occ[m_own][KING]; //king must be erased to properly add attacks along rays    
    for (int type {1}; type < NONE; type++)
    {
        uint64 occ {m_occ[m_enemy][type]};
        while (occ != 0)
        {
            uint8 sq = {bitScanAndReset(occ)};
            m_enemyAttackSet |= m_LUTData.getAttackSet(static_cast<EType>(type), sq, m_all);
        }
    }
    m_all |= m_occ[m_own][KING]; //retrieve the king    
}

void CMoveGenerator::addLegalMoves(const int sq, const EType type, const uint64 allowed) const
{
    uint64 set { m_LUTData.getAttackSet(type, sq, m_all) & allowed };
    while (set != 0)
    {
        const uint8 destSq {bitScanAndReset(set, m_own)};
        if (1ULL << destSq & m_fullOcc[m_enemy]) //this is a capture
        {
            //below strategy of grading even en prise captures with diff. seems better in more scenarios than commented en prise captures
            //anyway implement SEE in the future...

            //if (m_enemyAttackSet & 1ULL << a_sqDest) //enemy's piece is covered by something
            {
                m_pMoveList->insertCapture(sq, destSq, type, getEnemyType(destSq)); //grade basing on a direct exchange only
            }
            //else //capturing for free
            //{
            //    m_moveList->push_back(sq, a_sqDest, getEnemyType(a_sqDest));
            //}
        }
        else //no capturing
        {
            m_pMoveList->insertMove(sq, destSq, type);
        }
    }
}

void CMoveGenerator::addSinglePawnMoves(const int sq, const uint64 allowed) const
{
    const uint64 pawn {1ULL << sq};
    const uint64 enemy { (m_fullOcc[m_enemy] | m_EPSq) & allowed };
    uint64 set { m_LUTData.getPawnMask(sq, m_own) & enemy }; //handle captures
    while (set != 0)
    {
        uint8 destSq = bitScanAndReset(set, m_own);
        if (1ULL << destSq == m_EPSq)
        {
            m_pMoveList->insertEnPassant(sq, destSq);
        }
        else
        {
            m_pMoveList->insertCapture(sq, destSq, PAWN, getEnemyType(destSq));
        }
    }
    uint64 empty = {~m_all & allowed};
    const int shift = {8 - (m_own << 4)};
    if (pawn & _rotr64(empty, shift)) //handle a push 1fwd
    {
        m_pMoveList->insertMove(sq, sq + shift, PAWN);
    }
    if (pawn & RANK_2Fwd[m_own] & _rotr64(~m_all, shift) & _rotr64(empty, shift * 2)) //handle a push 2fwd
    {
        m_pMoveList->insertMove(sq, sq + shift * 2, PAWN);
    }
}

bool CMoveGenerator::hasPawnLegalMove(const int sq, const uint64 allowed) const
{
    const uint64 enemy { (m_fullOcc[m_enemy] | m_EPSq) & allowed };
    if ((m_LUTData.getPawnMask(sq, m_own) & enemy) != 0) //captures
    {
        return true;
    }

    const uint64 pawn {1ULL << sq};
    const uint64 empty = {~m_all & allowed};
    const int shift = {8 - (m_own << 4)};
    if (pawn & _rotr64(empty, shift)) //push 1fwd
    {
        return true;
    }
    if (pawn & RANK_2Fwd[m_own] & _rotr64(~m_all, shift) & _rotr64(empty, shift * 2)) //push 2fwd
    {
        return true;
    }
    return false;
}

void CMoveGenerator::addPawnsCaptures(uint64 set, const int offset) const
{
    while (set != 0)
    {
        const uint8 sq {bitScanAndReset(set)};
        const int destSq {sq + offset};
        if (1ULL << destSq == m_EPSq)
        {
            m_pMoveList->insertEnPassant(sq, destSq);
        }
        else
        {
            m_pMoveList->insertCapture(sq, destSq, PAWN, getEnemyType(destSq));
        }
    }
}

void CMoveGenerator::addPawnsPushes(uint64 set, const int offset) const
{
    while (set != 0)
    {
        const uint8 sq {bitScanAndReset(set)};
        m_pMoveList->insertMove(sq, sq + offset, PAWN);
    }
}

void CMoveGenerator::preventHorizontallyPinnedEP()
{
    const int EPFile {bitScan(m_EPSq) & 7};
    if (m_king >> 3 == 4 - m_own  &&  m_EPSq != 0  &&  EPFile > 0 && EPFile < 7)
    {
        const int pawnRank {4 - m_own};
        int leftFile {-1};
        int rightFile {-1};
        if (1ULL << (EPFile - 1 + pawnRank * 8) & m_occ[m_own][PAWN])
        {
            leftFile = EPFile - 1; //a pawn is available to enPassant on the left adjacent file
        }
        if (1ULL << (EPFile + 1 + pawnRank * 8) & m_occ[m_own][PAWN])
        {
            rightFile = EPFile + 1; //a pawn is available to enPassant on the right adjacent file
        }

        //if no pawns to EP - nothing to do, if 2 pawns can EP (from both sides) neither EP is pinned anyway
        if (leftFile == -1 && rightFile != -1 || leftFile != -1 && rightFile == -1)
        {
            const int sq { (leftFile != -1 ? leftFile : rightFile) + pawnRank * 8 };
            m_all ^= 1ULL << (EPFile + pawnRank * 8); //temporarily remove 2Fwd enemy pawn
            const uint64 orthSliders { m_occ[m_enemy][QUEEN] | m_occ[m_enemy][ROOK] };
            const uint64 possiblePins { m_LUTData.getRookAttackSet(m_king, m_all) & m_fullOcc[m_own] };
            uint64 pinners { m_LUTData.getRookAttackSet(m_king, m_all&~possiblePins) & orthSliders };
            while (pinners != 0 && possiblePins != 0)
            {
                uint8 pinner {bitScanAndReset(pinners, m_own)};
                const uint64 xRayLine {m_LUTData.getXRayLine(pinner, m_king)};
                if (1ULL << sq & xRayLine) //if the pawn is on the xRayline enPassant is illegal
                {
                    m_EPSq = 0;
                    break;
                }
            }
            m_all |= 1ULL << (EPFile + pawnRank * 8); //retrieve 2Fwd enemy pawn
        }
    }
}

void CMoveGenerator::updatePinnedPieces(uint64 & done) const
{
    const uint64 orthSliders { m_occ[m_enemy][QUEEN] | m_occ[m_enemy][ROOK] };
    uint64 possiblePins { m_LUTData.getRookAttackSet(m_king, m_all) & m_fullOcc[m_own] };
    uint64 pinners { m_LUTData.getRookAttackSet(m_king, m_all&~possiblePins) & orthSliders };

    const uint64 diagSliders { m_occ[m_enemy][QUEEN] | m_occ[m_enemy][BISHOP] };
    possiblePins = m_LUTData.getBishopAttackSet(m_king, m_all) & m_fullOcc[m_own];
    pinners |= m_LUTData.getBishopAttackSet(m_king, m_all&~possiblePins) & diagSliders;

    while (pinners != 0)
    {
        const uint8 pinner {bitScanAndReset(pinners, m_own)};
        const uint64 xRayLine {m_LUTData.getXRayLine(pinner, m_king)};
        const uint8 pin {bitScan(xRayLine & m_fullOcc[m_own])};
        const EType type {getType(pin)};
        if (PAWN == type)
        {
            addSinglePawnMoves(pin, xRayLine);
        }
        else
        {
            addLegalMoves(pin, type, xRayLine);
        }
        done |= 1ULL << pin; //mark that this piece was already updated (and must not be processed in updateRemainingPieces())        
    }
}

void CMoveGenerator::updateRemainingPieces(const uint64 done) const
{
    //pawns to go first
    const uint64 pawns {m_occ[m_own][PAWN] & ~done};
    const uint64 enemy {m_fullOcc[m_enemy] | m_EPSq};
    const uint64 empty {~m_all};
    const int shift = {8 - (m_own << 4)};
    addPawnsCaptures(pawns & NOT_A_FILE & _rotr64(enemy, shift - 1), shift - 1); //captures to the left
    addPawnsCaptures(pawns & NOT_H_FILE & _rotr64(enemy, shift + 1), shift + 1); //captures to the right
    addPawnsPushes(pawns & _rotr64(empty, shift), shift); //pushes 1fwd
    addPawnsPushes(pawns & RANK_2Fwd[m_own] & _rotr64(empty, shift) & _rotr64(empty, shift * 2), shift * 2); //pushes 2fwd

    //the rest of pieces
    for (int type {KNIGHT}; type < KING; type++)
    {
        uint64 occ {m_occ[m_own][type] & ~done};
        while (occ != 0)
        {
            uint32 sq {bitScanAndReset(occ, m_own)};
            addLegalMoves(sq, static_cast<EType>(type), ~m_fullOcc[m_own]);
        }
    }
}

void CMoveGenerator::updateCastling() //TODO: disallow castling after rook's captured and the other takes its place
{
    if (m_castlRights & 0x2 >> m_own && !(m_enemyAttackSet >> (m_king - 4) & 0x0C) //0xC0 not - 4 ??
        && !(m_all >> (m_king - 4) & 0x0E) && m_occ[m_own][ROOK] >> (m_king - 4) & 0x01)
    {
        m_pMoveList->insertMove(m_king, m_king - 2, KING);
    }

    if (m_castlRights & 0x8 >> m_own && !(m_enemyAttackSet >> (m_king - 4) & 0x60)
        && !(m_all >> (m_king - 4) & 0x60) && m_occ[m_own][ROOK] >> (m_king - 4) & 0x80)
    {
        m_pMoveList->insertMove(m_king, m_king + 2, KING);
    }
}

uint64 CMoveGenerator::getPinnedPieces() const
{
    const uint64 orthSliders { m_occ[m_enemy][QUEEN] | m_occ[m_enemy][ROOK] };
    uint64 possiblePins { m_LUTData.getRookAttackSet(m_king, m_all) & m_fullOcc[m_own] };
    uint64 pinners { m_LUTData.getRookAttackSet(m_king, m_all&~possiblePins) & orthSliders };

    const uint64 diagSliders { m_occ[m_enemy][QUEEN] | m_occ[m_enemy][BISHOP] };
    possiblePins = m_LUTData.getBishopAttackSet(m_king, m_all) & m_fullOcc[m_own];
    pinners |= m_LUTData.getBishopAttackSet(m_king, m_all&~possiblePins) & diagSliders;

    uint64 xRayLines {0};
    while (pinners != 0)
    {
        const uint8 pinner {bitScanAndReset(pinners, m_own)};
        xRayLines |= m_LUTData.getXRayLine(pinner, m_king);
    }
    return xRayLines & m_fullOcc[m_own];
}

void CMoveGenerator::updateCheckDefence(const uint64 checker) const
{
    const uint64 checkLine { m_LUTData.getXRayLine(bitScan(checker), m_king) };
    uint64 occ { m_fullOcc[m_own] & ~m_occ[m_own][KING] & ~getPinnedPieces() };
    while (occ != 0) //iterate over own pieces (except king and pins)
    {
        const uint8 sq {bitScanAndReset(occ, m_enemy)};
        const EType type {getType(sq)};
        if (PAWN == type)
        {
            addSinglePawnMoves(sq, checkLine);
        }
        else
        {
            addLegalMoves(sq, type, checkLine); //if (set & checkLine) the piece can block check or capture the checker
        }

        if (m_EPSq != 0 && _rotr64(m_EPSq, 8 - (m_own << 4)) & checkLine &&  //if 2-forward pawn causes a direct check...
            1ULL << sq & m_occ[m_own][PAWN] && m_LUTData.getPawnMask(sq, m_own) & m_EPSq) //and en passant is available...
        {
            m_pMoveList->insertEnPassant(sq, bitScan(m_EPSq)); //this en passant legally cancels the check
        }
    }
}

bool CMoveGenerator::isCheckDefencePossible(const uint64 checker) const
{
    const uint64 checkLine { m_LUTData.getXRayLine(bitScan(checker), m_king) };
    uint64 occ { m_fullOcc[m_own] & ~m_occ[m_own][KING] & ~getPinnedPieces() };
    while (occ != 0) //iterate over own pieces (except king and pins)
    {
        const uint8 sq {bitScanAndReset(occ)};
        const EType type {getType(sq)};
        if (PAWN == type && hasPawnLegalMove(sq, checkLine))
        {
            return true;
        }
        if (type != PAWN && m_LUTData.getAttackSet(type, sq, m_all) & checkLine)
        {
            return true;
        }
        if (m_EPSq != 0 && _rotr64(m_EPSq, 8 - (m_own << 4)) & checkLine && //if 2-forward pawn causes direct check...
            1ULL << sq & m_occ[m_own][PAWN] && m_LUTData.getPawnMask(sq, m_own) & m_EPSq) //and en passant is available
        {
            return true;
        }
    }
    return false;
}

void CMoveGenerator::updateKing()
{
    uint64 set { m_LUTData.getKingAttackSet(m_king, m_fullOcc[m_own]) & ~m_fullOcc[m_own] & ~m_enemyAttackSet };
    while (set != 0)
    {
        const uint8 destSq {bitScanAndReset(set, m_own)};
        if (1ULL << destSq & m_fullOcc[m_enemy]) //this is a capture
        {
            m_pMoveList->insertCapture(m_king, destSq, KING, getEnemyType(destSq));
        }
        else //no capture
        {
            m_pMoveList->insertMove(m_king, destSq, KING);
        }
    }
}

}//namespace fool


