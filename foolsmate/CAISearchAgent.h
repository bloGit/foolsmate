/*Welcome!
  This is a simple and purely educational chess engine named "foolsmate".
  For best move search it uses the negamax algorithm with alpha-beta pruning + transposition table,
  iterative deepening, simple move ordering (hashmove, captures, killermove and history moves),
  and basic position score evaluation (material + PST).
  Move generation is bitboard-based with precalculated attack sets and magic numbers (for xray pieces).
  The only chess-specific source of knowledge: www.chessprogramming.org;

  In the code (identifiers/comments) you may find a few abbreviations/acronyms listed below:
     sq - square (on a chessboard), usually uint8 with a value <0, 63>
    occ - occupancy: the exact arrangement of chess pieces on the chessboard in a given moment;
          always expressed by a bitboard (uint64) or a set of bitboards, e.g. all pieces, only one type, only one color, etc
    pos - (game) position
    set - usually an attack set (or move set) as a bitboard, possibly other 'bitboard subset'
   1Fwd - normal pawn movement (a push) by 1 square forward
   2Fwd - a push by 2 squares forward (possible only from the initial position)
   EPSq - en passant square (the one on which the attaking pawn rests after the capture)
2FR/3FR - derived from Three-Fold-Repetition rule
          which causes draw after 3 identical positions (or just 2 for the sake of AI search)
 AI/CAI - (class) artificial intelligence - best move search component / CAISearchAgent object / 'AI search'
     TT - transposition table    
    LUT - look up table (CLookupData object, which serves to moveGen)
    PST - piece square tables (used for score evaluation)    
*/

#ifndef FOOLSMATE_CAISEARCHAGENT
#define FOOLSMATE_CAISEARCHAGENT

#include <array>
#include <memory>
#include "CGameState.h"
#include "CTranspositionTable.h"

namespace fool
{

class CAISearchAgent
{    
    public:
    //************************ PUBLIC METHODS: **************************

    CAISearchAgent();    

    //terminates search on demand;
    void abortSearch();
    bool isSearchAborted() const;

    //starts best move search with iterative deepening mode,
    //it means it runs a tree search for subsequent depths from 1 to passed searchDepth (the external loop);
    //for each depth it iterates over moves of the root node of the game tree (the internal loop);
    //for each move being explored it calls negamax() to proceed with deeper (recursive) search;
    SMove runNegamax(int searchDepth, CGameState & gameState);


    private:
    //************************ PRIVATE METHODS: **************************

    //recursive negamax, the core of AI search; main steps are:
    //1. check the ultimate stop condition (depth 0 reached), if met it obtains an evaluation score from CGameState
    //   and returns it to parent node;
    //2. check if there's a draw position (by 2FR, material or 50move), return 0 if so (as most nautral score, which a draw is indeed);
    //3. check if node's chess position already exists in TT, if score is applicable return it,
    //   if not (e.g. not evaluated at an expected depth) but hashMove is stored for this position get the hashmove for move ordering;
    //4. obtain legal movelist for node's position from CGameState (they are being generated at this step by moveGen),
    //   it may be game over score (checkmate or draw) so it returns it then;
    //5. order the movelist passing to the function hashmove, killermove and history moves array for relevant color;
    //in a loop:
    //6. make subsequent moves from the movelist and after each call deeper negamax to analyze further positions in the tree;
    //7. if score returned by deeper negamax is <= beta perform a beta cutoff,
    //   store fresh killer move + update corresponding history move grade;
    //8. adjust up-to-now bestScore value and (if needed) also alpha + bestMove (on behalf of a new hashmove of examined position);
    //9. whichever path is taken (beta cutoff or the loop end) store data for this position as an entry in TT;
    sint16 negamax(const int depth, int alpha, const int beta);

    //resets several members that need to be reset before each best move request;
    void resetData();

    //extends the ongoing search by increasing target depth if a search tree is quite narrow (quick),
    //useful especially in very end-game for better mate following which otherwise would be beyond a horizon
    //(deeper than original depth limit) causing engine to play more stupid;
    //it denotes that the 'target depth' in the engine is not actually a target depth but a MINIMAL target depth;
    void handleTargetDepthIncrease(sint16 bestScore);
    
    //PERFT/DIVIDE - move generation correctness test;
    //it avoids all search optimizations to traverse the whole tree and count all leaf nodes (to a given depth);
    //then it may be compared to reference PERFT results (e.g. stockfish), if the node count differs there's an error;
    uint64 perft(int depth);


    //************************* DATA MEMBERS: ***************************

    //used by handleTargetDepthIncrease() to determine if examined leaf-nodes number is lesser than this;
    //if yes it means the up-to-now tree has been quite small/narrow so must have taken little time and
    //it is worth to increase the target search depth to obtain a stronger best move;
    //currently the value is arbitrarly set to reasonably assist (i.e. increase) during tests with the terget depth == 9;    
    static const int DEPTH_INCR_TREE_SIZE_THRESHOLD {3000000};

    //transposition table used to remember score of positions that were already evaluated and hashmove
    //(best move found) of positions for move ordering (if saved score is not applicable to be returned);
    CTranspositionTable m_TTable;

    //array of movelists, each movelist is destined for a separate depth of search tree
    //so simultaneously several movelists (as many as plies of tree that are in the current path) are exploited;
    std::unique_ptr<CMoveArray[]> m_moveList {nullptr};

    //array of killer moves, one killer move for each ply is maintained now;
    std::array<SMove, MAX_SEARCH_DEPTH> m_killerMove;

    //array for history move grades
    std::array<uint32_array_64x64, COLORS> m_historyMoves;
    
    //final target depth of best move search (used in the last iteration of iterative deepening loop);
    //may be increased during a search if time-affordable (see handleTargetDepthIncrease);
    int m_targetDepth {0};

    //search depth of a current iteration of iterative deepening;
    int m_currDepth {0};
    
    //an observing pointer to a game state object on behalf which the AI search is being performed;
    CGameState* m_gameState {nullptr};

    //used for analysis/debugging but also by handleTargetDepthIncrease();
    uint64 m_leafNodesCount {0};

    //to terminate and unwind negamax as quick as possible;
    bool m_searchAborted {false};    
};

}//namespace fool

#endif //FOOLSMATE_CAISEARCHAGENT

