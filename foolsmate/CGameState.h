/*Welcome!
  This is a simple and purely educational chess engine named "foolsmate".
  For best move search it uses the negamax algorithm with alpha-beta pruning + transposition table,
  iterative deepening, simple move ordering (hashmove, captures, killermove and history moves),
  and basic position score evaluation (material + PST).
  Move generation is bitboard-based with precalculated attack sets and magic numbers (for xray pieces).
  The only chess-specific source of knowledge: www.chessprogramming.org;

  In the code (identifiers/comments) you may find a few abbreviations/acronyms listed below:
     sq - square (on a chessboard), usually uint8 with a value <0, 63>
    occ - occupancy, exact arrangement of chess pieces on the chessboard in a given moment;
          always expressed by a bitboard (uint64) or a set of bitboards, e.g. all pieces, only one type, only one color, etc
    pos - (game) position
    set - usually an attack set (or move set) as a bitboard, possibly other 'bitboard subset'
   1Fwd - normal pawn movement (a push) by 1 square forward
   2Fwd - a push by 2 squares forward (possible only from the initial position)
   EPSq - en passant square (the one which the attaking pawn occupies after en passant capture)
2FR/3FR - derived from Three-Fold-Repetition rule
          which causes draw after 3 identical positions (or just 2 for the sake of AI search)
 AI/CAI - (class) artificial intelligence - best move search component (CAISearchAgent object) / 'AI search'
     TT - transposition table    
    LUT - look up table (CLookupData object, which serves to moveGen)
    PST - piece square tables (used for score evaluation)
*/

#ifndef FOOLSMATE_CGAMESTATE
#define FOOLSMATE_CGAMESTATE

#include "CMoveGenerator.h"
#include "CMoveArray.h"
#include "CGameHistory.h"

namespace fool
{

class CGameState
{        
    public:
    //************************ PUBLIC METHODS: **************************

    CGameState();        

#ifdef ICBCOM
    bool operator==(const CGameState & a_rGameState) const;
#endif
            
    //puts all chess pieces on their initial squares
    void setStartPosition();

    //puts chess pieces in accordance with the layout provided by GUI (CEngineIO)
    void setPositionFromLayout(const std::array<SGUISquare, 64> & layout, const EColor colorToMove);
    
    //getter for position's representing hashkey which CAI needs to store in the TT;
    uint64 getHashKey() const;

    //getter for evaluation score of the position to be returned at leaf nodes of best move search
    sint16 getPositionScore(const int ply);
    
    //the overload called by CEngineIO to provide current legal moves to GUI
    CMoveArray & getLegalMoves();

    //the overload called by CAI during the search, it invokes the move generation itself
    //and adapts an end score (if it's the end of the game - which is known only after the generation)
    sint16 getLegalMoves(CMoveArray & moveList, const int ply);

    //checks if a position is repetead twice 2FR - in such case negamax returns (stops searching this path)
    bool isPositionRepeated();
    
    //first overload of move(), directly accepts the moves from a CAI-managed movelists;
    //called also from the other move() where it accepts a move from the current game state's movelist;
    void move(const SMove & move);

    //the other overload which is a GUI-biased wrapper of the above move() and much slower than that one;
    //function allows GUI (a player) to pass the chosen move coords (destination square and target square)
    //and looks for it in the current moveList in order to perform it by the 'core' move() and do additional processing;
    bool move(int from, int to);

    //move-corresponding 'unmove' called by CAI or by undoMove() (a GUI player);
    //it swaps colors and reload the previous SPosition from the history;
    void unmove();
    
    //a wrapper of 'unmove()' additionally must handle features for the sake of GUI and the consistent move path
    //(lastMove square colorizing / retrieval of EPSq after pawn's 2Fwd cancelling);
    void undoMove();

    //creates new piece chosen by a player during promotion
    void choosePiece(const EType piece);

    //getter for the full occupancy used by CEngineIO to 'render' GUI
    uint64_array_2x6 getOccupancy() const;
    
    //getter which allows GUI mark last move squares
    std::pair<uint8, uint8> getLastMove() const;
    
    //CEngineIO notifies GUI about the game result basing on a value returned by this getter
    EGameResult getGameResult() const;
    
    //getter lets CEngineIO check if the move is a promotion and if yes notify GUI with its callback
    //also to protect a correct IO call sequance in case of an ongoing promotion choice
    bool isPromotion() const;

    
    private:
    //************************ PRIVATE METHODS: **************************
            
    //populate all zobrist tables with random values from which a position hashkey is computed
    //(utilized by the TT and by the CGameHistory);
    static void initZobrist();

    //adds pieces material values to the PST to avoid addition 'on the fly';
    static void initPieceSquareTables();

    //if a custom position is arranged this one makes sure no castling rights are granted if a king/rooks are not on their initial squares;
    void resetInitialCastlingRights();    
    
    //switches the game phase from the middle game to the end game; redirects PST references to the PST 'end game' version;
    //it also involves recalculation of the current position score basing on values from the new (end game) subarray;
    void handleSwitchPhase();

    //adds/deletes a piece to/from the occupancy bitboards and update hash and score values accordingly;
    void addToOccupancy(const EColor color, const EType type, const int sq);
    void delFromOccupancy(const EColor color, const EType type, const int sq);

    //currently promotes only to QUEEN for simplicity and narrower search tree;
    void handlePromotion(const int to);
    
    //GUI addressed rules-compliant choice for a promoted piece (TO BE FINISHED!!!!!!!!!!)    
    void handlePromotion();           

    //checks if the movement is a castling and if yes moves relevant rook and/or resets castling rights of a king's or a rook's move;
    void handleCastling(const SMove & move);

    //switches current player's color after each half-move (each turn)
    void swapColors();

    //checks if there is mating material in the game, if no - there is a draw
    //mating material is considered as at least one pawn or one rook or one queen
    // - or number of pieces higher than 2 in at least one color;
    bool isMatingMaterial() const;

    //verifies if the game is over, first checking possible stalemate and checkmate (no legal moves),
    //then it checks if 3FR occurs, then if mating material still exists and finally if 50-move rule occurs;
    void handleGameOver();
    

    //************************* DATA MEMBERS: ***************************

    using uint64_array_2x6x64 = std::array< std::array< std::array<uint64, 64>, NONE>, COLORS>;

    //all zobrist data to manage the hashkey value:
    static uint64_array_2x6x64 m_zobrist;
    static uint64 m_blackToMove;
    static uint64 m_WLCastling; //TODO
    static uint64 m_WSCastling; //TODO
    static uint64 m_BLCastling; //TODO
    static uint64 m_BSCastling; //TODO
    static std::array<uint64, 8> m_enPassant; //TODO

    using sint16_array_2x2x6x64 = std::array< std::array< std::array< std::array<sint16, 64>, NONE>, COLORS>, 2>;
    
    //piece square tables;
    static sint16_array_2x2x6x64 m_PST;

    //movelist of the current game state only - for CEngineIO and CAI root node
    //(on deeper search depths CAI uses own separate depth-assigned movelists);
    CMoveArray m_moveList;
    
    //it holds all previous moves of the current game;
    //necessary to check 3FR/2FR repetition and to allow taking moves back;
    CGameHistory m_history;
    
    //component responsible for legal moves generation after each half-move;
    CMoveGenerator m_moveGen = CMoveGenerator(m_pos.occ, m_pos.castlRights);

    //struct holds all needed/reversible info about the position (a current game state itself);
    SPosition m_pos;    
        
    //only used by CEngineIO...
    EGameResult m_gameResult {IN_PROGRESS};

    //when true there is a promotion and player must choose a piece; TO BE REMOVED!!!!!!!
    bool m_isPromotion {false};
};

inline uint64 CGameState::getHashKey() const
{
    return m_pos.hashKey;
}

inline sint16 CGameState::getPositionScore(const int a_ply)
{
    if (m_moveGen.isCheckmate())
    {
        return MIN_SCORE + a_ply;
    }
    else //ignore draw scenarios for performance reason (only in leaf nodes of AI search)
    {
        //in accordance with negamax - return the score (benefit/loss) from the WHITE's point of view
        return m_moveGen.getOwnColor() == WHITE ? m_pos.score : -m_pos.score;
    }
}

inline bool CGameState::isPositionRepeated()
{
    return m_history.is2FRepetition(m_pos.hashKey);
}

inline void CGameState::unmove()
{
    swapColors();
    m_pos = m_history.popPosition();
}

inline void CGameState::swapColors()
{
    m_moveGen.swapColors();
    m_pos.hashKey ^= m_blackToMove;
}

}//namespace fool

#endif //FOOLSMATE_CGAMESTATE

