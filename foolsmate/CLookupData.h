/*Welcome!
  This is a simple and purely educational chess engine named "foolsmate".
  For best move search it uses the negamax algorithm with alpha-beta pruning + transposition table,
  iterative deepening, simple move ordering (hashmove, captures, killermove and history moves),
  and basic position score evaluation (material + PST).
  Move generation is bitboard-based with precalculated attack sets and magic numbers (for xray pieces).
  The only chess-specific source of knowledge: www.chessprogramming.org;

  In the code (identifiers/comments) you may find a few abbreviations/acronyms listed below:
     sq - square (on a chessboard), usually uint8 with a value <0, 63>
    occ - occupancy: the exact arrangement of chess pieces on the chessboard in a given moment;
          always expressed by a bitboard (uint64) or a set of bitboards, e.g. all pieces, only one type, only one color, etc
    pos - (game) position
    set - usually an attack set (or move set) as a bitboard, possibly other 'bitboard subset'
   1Fwd - normal pawn movement (a push) by 1 square forward
   2Fwd - a push by 2 squares forward (possible only from the initial position)
   EPSq - en passant square (the one on which the attaking pawn rests after the capture)
2FR/3FR - derived from Three-Fold-Repetition rule
          which causes draw after 3 identical positions (or just 2 for the sake of AI search)
 AI/CAI - (class) artificial intelligence - best move search component / CAISearchAgent object / 'AI search'
     TT - transposition table    
    PST - piece square tables (used for score evaluation)    
    LUT - look up tables (in CLookupData class, which serves to moveGen)
*/

#ifndef FOOLSMATE_CLOOKUPDATA
#define FOOLSMATE_CLOOKUPDATA

#include <array>
#include "CommonStuff.h"

namespace fool
{

class CLookupData
{
    public:
    //************************ PUBLIC METHODS: **************************

    CLookupData();
    
    //direct getters for attack/move sets of individual piece type:
    uint64 getPawnMask(const uint8 sq, const EColor color) const; //it is only for captures (pawns' pushes are generated on the fly)
    uint64 getKnightAttackSet(const uint8 sq, const uint64 occ) const;
    uint64 getBishopAttackSet(const uint8 sq, const uint64 occ) const;
    uint64 getRookAttackSet(const uint8 sq, const uint64 occ) const;
    uint64 getQueenAttackSet(const uint8 sq, const uint64 occ) const;
    uint64 getKingAttackSet(const uint8 sq, const uint64 occ) const;
        
    //'generic' getter with a function pointer (if a specific piece type is a parameter)
    uint64 getAttackSet(const EType type, const uint8 sq, const uint64 occ) const;
    
    //returns a bitboard containing an 'attacking line' - all squares between an attacker
    //(depending on application - a checker or a pinner) and the king (including the attacker / excluding the king);
    //to keep uniform/branchless code in check processing it also returns KNIGHT's square if knight's move is passed
    //(so not necessarily an 'xRay' if a knight is involved);
    uint64 getXRayLine(const uint8 sq1, const uint8 sq2) const;    


    private:
    //************************ PRIVATE METHODS: **************************
    
    //initialization functions for attack/move sets of non-xray pieces;
    //they are named '..AttackMask' but they are effectively ready attack/move sets for these pieces:
    uint64 pawn_generateAttackMask(const uint8 sq, const EColor color) const;
    uint64 knight_generateAttackMask(const uint8 sq) const;
    uint64 king_generateAttackMask(const uint8 sq) const;
    
    //initialization functions for attack/move sets of x-ray pieces (rook and bishop, queen is a union of both):
    //ROOK:
    void rook_generateMasks();
    uint64 rook_generateOccupancyVariant(const uint8 sq, const int val) const;
    uint64 rook_generateAttackSet(const uint8 sq, const uint64 occupancy) const;
    //BISHOP:
    void bishop_generateMasks();
    uint64 bishop_generateOccupancyVariant(const uint8 sq, const int val) const;
    uint64 bishop_generateAttackSet(const uint8 sq, const uint64 occupancy) const;
    
    //generates magic numbers by 'trial and error' which are used for faster acquisition of attackSet index for xray pieces;
    void generateMagicNumbers(const bool rook);    

    //initializes m_line LUT;
    uint64 generateXRayLine(const uint8 sq1, const uint8 sq2) const;

    //triggers all LUT initializations
    void initializeLUT();        

    //assigns attackSet getters to corresponding items in the function pointers array
    //for the sake of 'generic' getAttackSet() getter;
    void assignFunctionPointers();

    //prints a taken bitboard in a console for testing/debugging purposes
    void printBitboard(uint64 bitBoard) const;
    

    //************************* DATA MEMBERS: ***************************    

    static const uint64 A_FILE {0x0101010101010101};
    static const uint64 H_FILE {0x8080808080808080};
    static const uint64 MAIN_DIAG {0x8040201008040201};
    static const uint64 ANTI_DIAG {0x0102040810204080};
    static const uint64 Rsh7_ANTI_DIAG {0x0002040810204081};

    //this value is imposed by a rook standing in the very corner of a chessboard because then
    //a largest - 12 bits - attack mask is involved and 2^12 (4096) different occupancies are possible
    //and may come from the game to request an attack set of a corner-rook;
    static const int MAX_OCC_PER_SQUARE {4096};
    
    //actual LUT arrays of attack/move sets:
    std::array< std::array<uint64, COLORS>, 64> m_pawnAttackMask;
    std::array<uint64, 64> m_knightAttackMask;
    std::array<uint64, 64> m_kingAttackMask;
    
    std::array<uint64, 64> m_rookAttackMask;
    std::array<uint64, 64> m_rookMagicNumber;
    std::array< std::array<uint64, MAX_OCC_PER_SQUARE>, 64> m_rookAttackSet;

    std::array<uint64, 64> m_bishopAttackMask;
    std::array<uint64, 64> m_bishopMagicNumber;
    std::array< std::array<uint64, 512>, 64> m_bishopAttackSet; //512 - bishop's max occupancies per square
    
    std::array< std::array<uint64, 64>, 64> m_line;

    //alias for a member-function pointer compatible with getXxxxAttakSet getters
    using getAttackSet_fPtr = uint64 (CLookupData::*)(uint8, uint64) const;

    //array of function pointers which allows a 'lookup' call (piece's EType is a relevant getter's index);
    //raw function pointers work faster than std::function's in this case;
    //they also seem a little bit faster than a switch-case approach;
    std::array<getAttackSet_fPtr, NONE> m_getAttackSet_fPtr;
};

inline uint64 CLookupData::getPawnMask(const uint8 sq, const EColor color) const
{
    return m_pawnAttackMask[sq][color];
}

inline uint64 CLookupData::getKnightAttackSet(const uint8 sq, const uint64 occ) const
{
    return m_knightAttackMask[sq];
}

inline uint64 CLookupData::getBishopAttackSet(const uint8 sq, const uint64 occ) const
{
    const int index = (occ & m_bishopAttackMask[sq]) * m_bishopMagicNumber[sq] >> 55;
    return m_bishopAttackSet[sq][index];
}

inline uint64 CLookupData::getRookAttackSet(const uint8 sq, const uint64 occ) const
{
    const int index = (occ & m_rookAttackMask[sq]) * m_rookMagicNumber[sq] >> 52;
    return m_rookAttackSet[sq][index];
}

inline uint64 CLookupData::getQueenAttackSet(const uint8 sq, const uint64 occ) const
{
    const int index1 = (occ & m_rookAttackMask[sq]) * m_rookMagicNumber[sq] >> 52;
    const int index2 = (occ & m_bishopAttackMask[sq]) * m_bishopMagicNumber[sq] >> 55;
    return (m_rookAttackSet[sq][index1] | m_bishopAttackSet[sq][index2]);
}

inline uint64 CLookupData::getKingAttackSet(const uint8 sq, const uint64 occ) const
{
    return m_kingAttackMask[sq];
}

inline uint64 CLookupData::getAttackSet(const EType type, const uint8 sq, const uint64 occ) const
{   
    return (*this.*m_getAttackSet_fPtr[type])(sq, occ);
}

inline uint64 CLookupData::getXRayLine(const uint8 sq1, const uint8 sq2) const
{
    return m_line[sq1][sq2];
}

}//namespace fool

#endif //FOOLSMATE_CLOOKUPDATA