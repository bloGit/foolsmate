#include "CGameHistory.h"

namespace fool
{

#ifdef ICBCOM
bool CGameHistory::operator==(const CGameHistory & rHistory) const
{    
    for (int i = 0; i < m_size; i++)
    {
        if (m_array[i] != rHistory.m_array[i])
        {
            return false;
        }
    }
    return true;
}
#endif

int CGameHistory::size() const
{
    return m_size;
}

bool CGameHistory::is2FRepetition(uint64 hashKey) const
{
    if (m_size < 4 || m_array[m_size - 1].fiftyCount < 3)
    {
        return false;
    }
    for (int i {m_size - 4}; ; i -= 2)
    {
        if (m_array[i].hashKey == hashKey)
        {
            return true;
        }
        if (m_array[i].fiftyCount <= 1)
        {
            return false;
        }
    }
    return false;
}

bool CGameHistory::is3FRepetition(uint64 hashKey) const
{
    int repetitions = 1;
    if (m_size < 4 || m_array[m_size - 1].fiftyCount < 3)
    {
        return false;
    }
    for (int i {m_size - 4}; ; i -= 2) //start to look from 2 full moves earlier (4 halfmoves) and backwards every second halfmove
    {
        if (m_array[i].hashKey == hashKey)
        {
            if (REPETITION_LIMIT == ++repetitions)
            {
                return true;
            }
        }
        if (m_array[i].fiftyCount <= 1) //if 50-count reaches 0 (or 1 for odd) no need to look earlier in history (no repetition can occur before 50count reset)
        {
            break;
        }
    }
    return false;
}

std::pair<uint8, uint8> CGameHistory::getLastMove(const uint64_array_2x6 & occ, EColor color) const
{
    std::pair<uint8, uint8> lastMove {255, 255};
    for (int type {0}; m_size > 0 && type < NONE; type++)
    {
        const SPosition lastPos = m_array[m_size - 1];
        uint64 xor { lastPos.occ[color][type] ^ occ[color][type] }; //difference in occupancies - this type was moved
        if (xor != 0)
        {
            lastMove.first = bitScanAndReset(xor);
            lastMove.second = bitScanAndReset(xor);
            break;
        }
    }
    return lastMove;
}

uint8 CGameHistory::getEnPassantSquare(const uint64_array_2x6 & occ, EColor color) const
{    
    uint8 EPSq {0};
    if (m_size > 0)
    {
        const SPosition lastPos = m_array[m_size - 1];
        uint64 xor { lastPos.occ[color][PAWN] ^ occ[color][PAWN] }; //difference in occupancies - pawn was moved
        if (xor != 0)
        {
            uint8 sq1 {bitScanAndReset(xor)};
            uint8 sq2 {bitScanAndReset(xor)};
            if (16 == std::abs(sq1 - sq2)) //if 2-forward pawn's move is the latest in the history
            {
                EPSq = (sq1 + sq2) / 2; //compute the en passant square index
            }        
        }    
    }
    return EPSq;
}

}//namespace fool
