/*Welcome!
  This is a simple and purely educational chess engine named "foolsmate".
  For best move search it uses the negamax algorithm with alpha-beta pruning + transposition table,
  iterative deepening, simple move ordering (hashmove, captures, killermove and history moves),
  and basic position score evaluation (material + PST).
  Move generation is bitboard-based with precalculated attack sets and magic numbers (for xray pieces).
  The only chess-specific source of knowledge: www.chessprogramming.org;

  In the code (identifiers/comments) you may find a few abbreviations/acronyms listed below:
     sq - square (on a chessboard), usually uint8 with a value <0, 63>
    occ - occupancy: the exact arrangement of chess pieces on the chessboard in a given moment;
          always expressed by a bitboard (uint64) or a set of bitboards, e.g. all pieces, only one type, only one color, etc
    pos - (game) position
    set - usually an attack set (or move set) as a bitboard, possibly other 'bitboard subset'
   1Fwd - normal pawn movement (a push) by 1 square forward
   2Fwd - a push by 2 squares forward (possible only from the initial position)
   EPSq - en passant square (the one on which the attaking pawn rests after the capture)
2FR/3FR - derived from Three-Fold-Repetition rule
          which causes draw after 3 identical positions (or just 2 for the sake of AI search)
 AI/CAI - (class) artificial intelligence - best move search component / CAISearchAgent object / 'AI search'
     TT - transposition table    
    LUT - look up table (CLookupData object, which serves to moveGen)
    PST - piece square tables (used for score evaluation)    
*/

#ifndef FOOLSMATE_CENGINEIO
#define FOOLSMATE_CENGINEIO

#include <memory>
#include <functional>
#include "CGameState.h"
#include "CAISearchAgent.h"

namespace fool
{

class CEngineIO
{
    public:
    //************************ PUBLIC METHODS: **************************

    CEngineIO();    

    //following three functions are templates just for an easier notation of parameter types which are function pointers:
    //registers a GUI callback for updating GUI chessboard after every move;
    template<typename F> void registerCallback_redraw(F funPtr);

    //registers a GUI callback for handling game over;
    template<typename F> void registerCallback_gameOver(F funPtr);

    //registers a GUI callback for handling promotion;
    template<typename F> void registerCallback_promotion(F funPtr);
    
    //restarts the game - to set initial positions and redraw GUI;
    void resetGame(); 

    //allows GUI (a player indeed) to finally confirm the chosen destination square and
    //triggers all operations necessary to perform this movement;
    bool makeMove(int from, int to);

    //allows to take a move back to previous position;
    void takeBackMove();    

    //creates new piece chosen by a player during promotion;
    void choosePiece(EType piece);

    //quits the game (intension - player's resignation or draw agreement);
    //may be used by GUI or not but it provides triggering a gameOver callback along with clearing CGameState;
    void quitGame(EGameResult result);

    //resets CGamesState and makes it arrange pieces according to a custom layout;
    void setCustomPosition(const uint8* const ptr, EColor colorToMove);

    //triggers negamax algorithm of AI search to determine best move and performs this move;
    void makeBestMove(int searchDepth);
   

    private:
    //************************ PRIVATE METHODS: **************************

    //prepares data (the fresh layout) for GUI and triggers it with its own callback
    void redrawGUI();    
    

    //************************* DATA MEMBERS: ***************************

    //AI module - searches a game tree for the best move;
    CAISearchAgent m_bestMoveSeeker;
    
    //current game state;
    std::unique_ptr<CGameState> m_pGameState {std::make_unique<CGameState>()};

    //GUI callback for updating GUI's chessboard after every move;
    std::function<void (SGUISquare[64])> m_redraw;
           
    //GUI callback for handling game over;
    std::function<void (EGameResult)> m_gameOver;

    //GUI callback for handling promotion;
    std::function<void ()> m_promotion;
};

}//namespace fool

#endif //FOOLSMATE_CENGINEIO

