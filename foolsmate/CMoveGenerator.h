/*Welcome!
  This is a simple and purely educational chess engine named "foolsmate".
  For best move search it uses the negamax algorithm with alpha-beta pruning + transposition table,
  iterative deepening, simple move ordering (hashmove, captures, killermove and history moves),
  and basic position score evaluation (material + PST).
  Move generation is bitboard-based with precalculated attack sets and magic numbers (for xray pieces).
  The only chess-specific source of knowledge: www.chessprogramming.org;

  In the code (identifiers/comments) you may find a few abbreviations/acronyms listed below:
     sq - square (on a chessboard), usually uint8 with a value <0, 63>
    occ - occupancy: the exact arrangement of chess pieces on the chessboard in a given moment;
          always expressed by a bitboard (uint64) or a set of bitboards, e.g. all pieces, only one type, only one color, etc
    pos - (game) position
    set - usually an attack set (or move set) as a bitboard, possibly other 'bitboard subset'
   1Fwd - normal pawn movement (a push) by 1 square forward
   2Fwd - a push by 2 squares forward (possible only from the initial position)
   EPSq - en passant square (the one on which the attaking pawn rests after the capture)
2FR/3FR - derived from Three-Fold-Repetition rule
          which causes draw after 3 identical positions (or just 2 for the sake of AI search)
 AI/CAI - (class) artificial intelligence - best move search component / CAISearchAgent object / 'AI search'
     TT - transposition table    
    PST - piece square tables (used for score evaluation)    
    LUT - look up tables (in CLookupData class, which serves to moveGen)
*/

#ifndef FOOLSMATE_CMOVEGENERATOR
#define FOOLSMATE_CMOVEGENERATOR

#include "CMoveArray.h"
#include "CLookupData.h"

namespace fool
{

class CMoveGenerator
{           
    public:
    //************************ PUBLIC METHODS: **************************

    CMoveGenerator(const uint64_array_2x6 & pOccupancy, const uint8 & castlRights);
    
#ifdef ICBCOM //consistency check for debug purpose
    bool operator==(const CMoveGenerator & rGameState) const;
#endif

    //this is set to non-zero by CGameState each time a pawn moved 2Fwd in the latest move;
    //it denotes a possibility to generate en passant capture (for currently generated color);
    void setEnPassantSquare(uint8 enPassantSquare);

    //logically 'color' member fields could belong to CGameState direcly, however generator reads them much more extensively
    //so it is easier to keep them here and provide suitable accessors to the higher level (to CGameState class);
    EColor getOwnColor() const;
    EColor getEnemyColor() const;    
    void setCurrentColor(EColor color);
    void swapColors();

    //returns a number of pieces (population count) of a given color; since generator keeps unioned m_fullOcc
    //it is easier to get it from here than calculate by CGameState directly; used for mating material and game phase;
    uint8 getPopCount(EColor color) const;

    //this is the main function which triggers full legal move generation;
    //called by CGameState before each half-move of the game;
    void generateLegalMoves(CMoveArray & moveList);

    //verifies if there's a checkmate (a check occurs + no legal moves) while moves have not yet been generated;
    //logic scheme is roughly similar to move generateLegalMoves() but it is obviously lighter/faster and is used
    //in AI-search 0 depth (leaf nodes) to return a correct evaluation score in case of a checkmate;
    bool isCheckmate();

    //a check test;
    bool isCheck() const;


    private:    
    //************************ PRIVATE METHODS: **************************

    //in some places type of a piece (which is a bit on a bitboard) is not instantly known by the logic;
    //these functions determine the type by iterating through occ (I dislike this but never devised a cheap alternative);
    EType getType(const int sq) const;
    EType getEnemyType(const int sq) const;
    
    //verify if in a position a king is in check;
    uint64 determineCheck();

    //collects all squares (also with enemy-defended pieces) which are under attack by enemy color
    //and adds them to m_enemyAttackSet bitboard;
    void updateEnemyAttackSet();    

    //bit-scans already acquired set (a bitboard) of all legal moves of a single non-pawn piece
    //and adds them to the movelist as integer indices (from, to);
    void addLegalMoves(const int sq, const EType type, const uint64 allowed) const;

    //adds moves of a single pawn - captures are bitscanned from a mask, non-captures are done by 'runtime' bit shift;
    void addSinglePawnMoves(const int sq, const uint64 allowed) const;

    //logic scheme similar to above but it doesn't add moves, just verifies their existance, used by isCheckmate() procedure;
    bool hasPawnLegalMove(const int sq, const uint64 allowed) const;

    //bit-scans already provided collective pawns' occupancy only WITH available captures,
    //determines destination squares by passed offset and adds them to the movelist;
    void addPawnsCaptures(uint64 set, const int offset) const;
    
    //bit-scans provided collective pawns' occupancy WITH available non-captures, determines destinations like above;
    void addPawnsPushes(uint64 set, const int offset) const;
    
    //covers very specific case only when king is on 4th(5th) rank and en passant may be horizontally pinned;
    //in such case TWO pawns (the one which captures and the one to be captured) disappear from the rank at once
    //leaving possibly open attack line which would 'cheat' a regular pin-detection procedure;
    void preventHorizontallyPinnedEP();

    //detects pieces which are pinned (stand between xray enemy attacker and own king - so by stepping off the line
    //they would discover illegal check) and adds their moves only if they are legal (sticking along the attack line
    //or capturing the attacker, for example a bishop partially pinned by a bishop/queen, or a rook by a rook/queen, etc);
    void updatePinnedPieces(uint64 & done) const;

    //generates moves of all remaining (not pinned) 'normal' free pieces;
    //first it handles pawns 'on the fly' bitwise AND-ing legal destinations for all 4 move-categories/directions (offsets);
    //then it handles non-pawns: extracts (bitscan) individudal pieces from subsequent types and passes to addLegalMoves();
    void updateRemainingPieces(uint64 done) const;

    //adds castling(s) if legal;
    void updateCastling();
        
    //similar to updatePinnedPieces() but its purpose is not to update their moves but only gather them
    //to a single bitboard, used by both 'CheckDefence' methods to exclude pins from consideration altogether
    //(even partial pin cannot block a check);
    uint64 getPinnedPieces() const;

    //generates legal moves in case of a CHECK by trying subsequent (not pinned) candidates
    //which can step on the checkline (to block or capture the checker);
    void updateCheckDefence(const uint64 checker) const;

    //logic scheme as above but only tests whether there is ANY possible move against the check;
    bool isCheckDefencePossible(const uint64 checker) const;

    //generates moves of the king;
    void updateKing();


    //************************* DATA MEMBERS: ***************************

    static const uint64 NOT_A_FILE {0xfefefefefefefefe};
    static const uint64 NOT_H_FILE {0x7f7f7f7f7f7f7f7f};
    static const uint64 RANK_2Fwd[COLORS];

    //this is a precalculated attack/move sets (and x-ray lines) bitboards provider;
    static const CLookupData m_LUTData;

    //an observing pointer to a passed movelist to which the generator is supposed to add legal moves;    
    //since multiple methods use this it's a class member for convenience (not a reference param);
    CMoveArray * m_pMoveList {nullptr};

    //generator needs occupancies (owned by CGameState) regularly, hence a member reference for convenience;
    const uint64_array_2x6 & m_occ;

    //it also needs castling rights (owned by CGameState) for legal castling generation;
    const uint8 & m_castlRights;
    
    //in many places generator needs to refer to full occupancy of a color, it 'unions' them in this array;
    std::array<uint64, COLORS> m_fullOcc;
    
    //full occupancy of both colors also handy
    uint64 m_all {0};

    //full enemy attack set used for king's moves generation and a check test
    uint64 m_enemyAttackSet {0};
    
    //non-zero only when enemy's pawn has moved 2 forward in previous half-move;
    //zero if no pawn 2Fwd move has been made so no en passant capture possible;
    //it holds EP square as a bitboard (with bit at EP index set);
    uint64 m_EPSq {0};

    //the color of currently being updated (move-generated) pieces
    EColor m_own {WHITE};

    //the opposite color (the enemy of currently updated pieces)
    EColor m_enemy {BLACK};

    //king's square index often directly referred
    uint8 m_king {0};

};

inline void CMoveGenerator::setEnPassantSquare(uint8 EPSq)
{
    m_EPSq = EPSq != 0 ? 1ULL << EPSq : 0;
}

inline EColor CMoveGenerator::getOwnColor() const
{
    return m_own;
}

inline EColor CMoveGenerator::getEnemyColor() const
{
    return m_enemy;
}

inline void CMoveGenerator::swapColors()
{
    m_own = EColor(m_own ^ BLACK);
    m_enemy = EColor(m_enemy ^ BLACK);
}

inline uint8 CMoveGenerator::getPopCount(EColor color) const
{
    return static_cast<uint8>( __popcnt64(m_fullOcc[color]) );
}

inline bool CMoveGenerator::isCheck() const
{
    return (m_enemyAttackSet & m_occ[m_own][KING]) != 0;
}

}//namespace fool

#endif //FOOLSMATE_CMOVEGENERATOR

