﻿namespace foolGUI
{
    partial class CFPositionSetter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGenPos = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnDefault = new System.Windows.Forms.Button();
            this.txtName = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblLayoutName = new System.Windows.Forms.Label();
            this.cboxLayouts = new System.Windows.Forms.ComboBox();
            this.lblChoose = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.rbtn_white = new System.Windows.Forms.RadioButton();
            this.rbtn_black = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // btnGenPos
            // 
            this.btnGenPos.Location = new System.Drawing.Point(246, 499);
            this.btnGenPos.Name = "btnGenPos";
            this.btnGenPos.Size = new System.Drawing.Size(103, 23);
            this.btnGenPos.TabIndex = 0;
            this.btnGenPos.Text = "Generate position";
            this.btnGenPos.UseVisualStyleBackColor = true;
            this.btnGenPos.Click += new System.EventHandler(this.btnGenPos_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(136, 499);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(86, 23);
            this.btnClear.TabIndex = 1;
            this.btnClear.Text = "Clear all";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnDefault
            // 
            this.btnDefault.Location = new System.Drawing.Point(21, 499);
            this.btnDefault.Name = "btnDefault";
            this.btnDefault.Size = new System.Drawing.Size(90, 23);
            this.btnDefault.TabIndex = 2;
            this.btnDefault.Text = "Default layout";
            this.btnDefault.UseVisualStyleBackColor = true;
            this.btnDefault.Click += new System.EventHandler(this.btnDefault_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(125, 540);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(104, 20);
            this.txtName.TabIndex = 3;
            this.txtName.Text = "My layout";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(236, 539);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(79, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblLayoutName
            // 
            this.lblLayoutName.AutoSize = true;
            this.lblLayoutName.Location = new System.Drawing.Point(53, 543);
            this.lblLayoutName.Name = "lblLayoutName";
            this.lblLayoutName.Size = new System.Drawing.Size(71, 13);
            this.lblLayoutName.TabIndex = 5;
            this.lblLayoutName.Text = "Layout name:";
            // 
            // cboxLayouts
            // 
            this.cboxLayouts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxLayouts.FormattingEnabled = true;
            this.cboxLayouts.Location = new System.Drawing.Point(125, 569);
            this.cboxLayouts.Name = "cboxLayouts";
            this.cboxLayouts.Size = new System.Drawing.Size(104, 21);
            this.cboxLayouts.TabIndex = 7;
            this.cboxLayouts.TextChanged += new System.EventHandler(this.cboxLayouts_TextChanged);
            // 
            // lblChoose
            // 
            this.lblChoose.AutoSize = true;
            this.lblChoose.Location = new System.Drawing.Point(48, 572);
            this.lblChoose.Name = "lblChoose";
            this.lblChoose.Size = new System.Drawing.Size(77, 13);
            this.lblChoose.TabIndex = 8;
            this.lblChoose.Text = "Layout to load:";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(236, 569);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(79, 23);
            this.btnDelete.TabIndex = 9;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // rbtn_white
            // 
            this.rbtn_white.AutoSize = true;
            this.rbtn_white.Checked = true;
            this.rbtn_white.Location = new System.Drawing.Point(85, 471);
            this.rbtn_white.Name = "rbtn_white";
            this.rbtn_white.Size = new System.Drawing.Size(91, 17);
            this.rbtn_white.TabIndex = 10;
            this.rbtn_white.TabStop = true;
            this.rbtn_white.Text = "white to move";
            this.rbtn_white.UseVisualStyleBackColor = true;
            // 
            // rbtn_black
            // 
            this.rbtn_black.AutoSize = true;
            this.rbtn_black.Location = new System.Drawing.Point(191, 471);
            this.rbtn_black.Name = "rbtn_black";
            this.rbtn_black.Size = new System.Drawing.Size(92, 17);
            this.rbtn_black.TabIndex = 11;
            this.rbtn_black.Text = "black to move";
            this.rbtn_black.UseVisualStyleBackColor = true;
            // 
            // CFPositionSetter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 611);
            this.Controls.Add(this.rbtn_black);
            this.Controls.Add(this.rbtn_white);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.lblChoose);
            this.Controls.Add(this.cboxLayouts);
            this.Controls.Add(this.lblLayoutName);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.btnDefault);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnGenPos);
            this.Name = "CFPositionSetter";
            this.Text = "Position Generator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGenPos;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnDefault;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblLayoutName;
        private System.Windows.Forms.ComboBox cboxLayouts;
        private System.Windows.Forms.Label lblChoose;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.RadioButton rbtn_white;
        private System.Windows.Forms.RadioButton rbtn_black;

    }
}