﻿namespace foolGUI
{
    partial class CFMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {            
            this.pnlNotation = new System.Windows.Forms.Panel();
            this.pnlChessboard = new System.Windows.Forms.Panel();
            this.btnResign = new System.Windows.Forms.Button();
            this.btnMoveback = new System.Windows.Forms.Button();
            this.btnDraw = new System.Windows.Forms.Button();
            this.btnReady = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.lblReady = new System.Windows.Forms.Label();
            this.lblClockWhite = new System.Windows.Forms.Label();
            this.lblClockBlack = new System.Windows.Forms.Label();
            this.lblWhiteScore = new System.Windows.Forms.Label();
            this.lblBlackScore = new System.Windows.Forms.Label();
            this.lblColon = new System.Windows.Forms.Label();
            this.lblWhiteName = new System.Windows.Forms.Label();
            this.lblBlackName = new System.Windows.Forms.Label();
            this.pboxWhite = new System.Windows.Forms.PictureBox();
            this.pboxBlack = new System.Windows.Forms.PictureBox();
            this.pboxFrameWhite = new System.Windows.Forms.PictureBox();
            this.pboxFrameBlack = new System.Windows.Forms.PictureBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.rtxtChat = new System.Windows.Forms.RichTextBox();
            this.btnSetPos = new System.Windows.Forms.Button();
            this.cboxSearchDepth = new System.Windows.Forms.ComboBox();
            this.pnlRules = new System.Windows.Forms.Panel();
            this.btnSwap = new System.Windows.Forms.Button();
            this.checkMoveback = new System.Windows.Forms.CheckBox();
            this.radioTime60 = new System.Windows.Forms.RadioButton();
            this.radioTime30 = new System.Windows.Forms.RadioButton();
            this.radioTime20 = new System.Windows.Forms.RadioButton();
            this.radioTime15 = new System.Windows.Forms.RadioButton();
            this.radioTime10 = new System.Windows.Forms.RadioButton();
            this.radioTime5 = new System.Windows.Forms.RadioButton();
            this.radioTime3 = new System.Windows.Forms.RadioButton();
            this.pnlMode = new System.Windows.Forms.Panel();
            this.lblAI = new System.Windows.Forms.Label();
            this.lblOR2 = new System.Windows.Forms.Label();
            this.btnAI = new System.Windows.Forms.Button();
            this.btnCreate = new System.Windows.Forms.Button();
            this.btnJoin = new System.Windows.Forms.Button();
            this.lblServer = new System.Windows.Forms.Label();
            this.lblClient = new System.Windows.Forms.Label();
            this.lblIP = new System.Windows.Forms.Label();
            this.lblOR = new System.Windows.Forms.Label();
            this.txtIP = new System.Windows.Forms.TextBox();
            this.pnlNotation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxWhite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxBlack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxFrameWhite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxFrameBlack)).BeginInit();
            this.pnlRules.SuspendLayout();
            this.pnlMode.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlNotation
            // 
            this.pnlNotation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(125)))), ((int)(((byte)(20)))));
            this.pnlNotation.Controls.Add(this.pnlChessboard);
            this.pnlNotation.Location = new System.Drawing.Point(37, 37);
            this.pnlNotation.Name = "pnlNotation";
            this.pnlNotation.Size = new System.Drawing.Size(690, 690);
            this.pnlNotation.TabIndex = 64;
            // 
            // pnlChessboard
            // 
            this.pnlChessboard.BackColor = System.Drawing.Color.SaddleBrown;
            this.pnlChessboard.Location = new System.Drawing.Point(18, 18);
            this.pnlChessboard.Name = "pnlChessboard";
            this.pnlChessboard.Size = new System.Drawing.Size(654, 654);
            this.pnlChessboard.TabIndex = 0;
            // 
            // btnResign
            // 
            this.btnResign.Enabled = false;
            this.btnResign.Location = new System.Drawing.Point(750, 508);
            this.btnResign.Name = "btnResign";
            this.btnResign.Size = new System.Drawing.Size(93, 23);
            this.btnResign.TabIndex = 66;
            this.btnResign.Text = "Resign game";
            this.btnResign.UseVisualStyleBackColor = true;
            // 
            // btnMoveback
            // 
            this.btnMoveback.Enabled = false;
            this.btnMoveback.Location = new System.Drawing.Point(960, 508);
            this.btnMoveback.Name = "btnMoveback";
            this.btnMoveback.Size = new System.Drawing.Size(96, 23);
            this.btnMoveback.TabIndex = 68;
            this.btnMoveback.Text = "Take move back";
            this.btnMoveback.UseVisualStyleBackColor = true;
            this.btnMoveback.Click += new System.EventHandler(this.btnMoveback_Click);
            // 
            // btnDraw
            // 
            this.btnDraw.Enabled = false;
            this.btnDraw.Location = new System.Drawing.Point(855, 508);
            this.btnDraw.Name = "btnDraw";
            this.btnDraw.Size = new System.Drawing.Size(93, 23);
            this.btnDraw.TabIndex = 71;
            this.btnDraw.Text = "Offer a draw";
            this.btnDraw.UseVisualStyleBackColor = true;
            // 
            // btnReady
            // 
            this.btnReady.Enabled = false;
            this.btnReady.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnReady.Location = new System.Drawing.Point(848, 385);
            this.btnReady.Name = "btnReady";
            this.btnReady.Size = new System.Drawing.Size(100, 23);
            this.btnReady.TabIndex = 72;
            this.btnReady.Text = "READY";
            this.btnReady.UseVisualStyleBackColor = true;            
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(761, 34);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(113, 18);
            this.lblName.TabIndex = 93;
            this.lblName.Text = "Enter your name:";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblReady
            // 
            this.lblReady.Enabled = false;
            this.lblReady.Location = new System.Drawing.Point(794, 410);
            this.lblReady.Name = "lblReady";
            this.lblReady.Size = new System.Drawing.Size(211, 18);
            this.lblReady.TabIndex = 78;
            this.lblReady.Text = "Click READY to start the game.";
            this.lblReady.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblClockWhite
            // 
            this.lblClockWhite.AutoSize = true;
            this.lblClockWhite.BackColor = System.Drawing.Color.Transparent;
            this.lblClockWhite.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblClockWhite.Location = new System.Drawing.Point(804, 480);
            this.lblClockWhite.Name = "lblClockWhite";
            this.lblClockWhite.Size = new System.Drawing.Size(39, 16);
            this.lblClockWhite.TabIndex = 82;
            this.lblClockWhite.Text = "00:00";
            // 
            // lblClockBlack
            // 
            this.lblClockBlack.AutoSize = true;
            this.lblClockBlack.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblClockBlack.Location = new System.Drawing.Point(959, 480);
            this.lblClockBlack.Name = "lblClockBlack";
            this.lblClockBlack.Size = new System.Drawing.Size(39, 16);
            this.lblClockBlack.TabIndex = 83;
            this.lblClockBlack.Text = "00:00";
            // 
            // lblWhiteScore
            // 
            this.lblWhiteScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblWhiteScore.Location = new System.Drawing.Point(873, 460);
            this.lblWhiteScore.Name = "lblWhiteScore";
            this.lblWhiteScore.Size = new System.Drawing.Size(28, 16);
            this.lblWhiteScore.TabIndex = 102;
            this.lblWhiteScore.Text = "0";
            this.lblWhiteScore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBlackScore
            // 
            this.lblBlackScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblBlackScore.Location = new System.Drawing.Point(900, 460);
            this.lblBlackScore.Name = "lblBlackScore";
            this.lblBlackScore.Size = new System.Drawing.Size(28, 16);
            this.lblBlackScore.TabIndex = 103;
            this.lblBlackScore.Text = "0";
            this.lblBlackScore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblColon
            // 
            this.lblColon.AutoSize = true;
            this.lblColon.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblColon.Location = new System.Drawing.Point(895, 459);
            this.lblColon.Name = "lblColon";
            this.lblColon.Size = new System.Drawing.Size(12, 16);
            this.lblColon.TabIndex = 104;
            this.lblColon.Text = ":";
            this.lblColon.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWhiteName
            // 
            this.lblWhiteName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblWhiteName.Location = new System.Drawing.Point(758, 436);
            this.lblWhiteName.Name = "lblWhiteName";
            this.lblWhiteName.Size = new System.Drawing.Size(130, 21);
            this.lblWhiteName.TabIndex = 86;
            this.lblWhiteName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBlackName
            // 
            this.lblBlackName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblBlackName.Location = new System.Drawing.Point(913, 436);
            this.lblBlackName.Name = "lblBlackName";
            this.lblBlackName.Size = new System.Drawing.Size(130, 22);
            this.lblBlackName.TabIndex = 87;
            this.lblBlackName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pboxWhite
            // 
            this.pboxWhite.BackColor = System.Drawing.Color.White;
            this.pboxWhite.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pboxWhite.Location = new System.Drawing.Point(778, 460);
            this.pboxWhite.Name = "pboxWhite";
            this.pboxWhite.Size = new System.Drawing.Size(90, 15);
            this.pboxWhite.TabIndex = 84;
            this.pboxWhite.TabStop = false;
            // 
            // pboxBlack
            // 
            this.pboxBlack.BackColor = System.Drawing.Color.Black;
            this.pboxBlack.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pboxBlack.Location = new System.Drawing.Point(932, 461);
            this.pboxBlack.Name = "pboxBlack";
            this.pboxBlack.Size = new System.Drawing.Size(90, 15);
            this.pboxBlack.TabIndex = 85;
            this.pboxBlack.TabStop = false;
            // 
            // pboxFrameWhite
            // 
            this.pboxFrameWhite.BackColor = System.Drawing.Color.Transparent;
            this.pboxFrameWhite.Location = new System.Drawing.Point(775, 457);
            this.pboxFrameWhite.Name = "pboxFrameWhite";
            this.pboxFrameWhite.Size = new System.Drawing.Size(96, 21);
            this.pboxFrameWhite.TabIndex = 88;
            this.pboxFrameWhite.TabStop = false;
            // 
            // pboxFrameBlack
            // 
            this.pboxFrameBlack.BackColor = System.Drawing.Color.Transparent;
            this.pboxFrameBlack.Location = new System.Drawing.Point(929, 458);
            this.pboxFrameBlack.Name = "pboxFrameBlack";
            this.pboxFrameBlack.Size = new System.Drawing.Size(96, 21);
            this.pboxFrameBlack.TabIndex = 89;
            this.pboxFrameBlack.TabStop = false;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(760, 52);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(115, 20);
            this.txtName.TabIndex = 92;
            this.txtName.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtName_KeyUp);
            // 
            // txtMessage
            // 
            this.txtMessage.Enabled = false;
            this.txtMessage.Location = new System.Drawing.Point(752, 707);
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(304, 20);
            this.txtMessage.TabIndex = 91;
            // 
            // rtxtChat
            // 
            this.rtxtChat.BackColor = System.Drawing.Color.White;
            this.rtxtChat.Enabled = false;
            this.rtxtChat.Location = new System.Drawing.Point(751, 544);
            this.rtxtChat.Name = "rtxtChat";
            this.rtxtChat.ReadOnly = true;
            this.rtxtChat.Size = new System.Drawing.Size(304, 147);
            this.rtxtChat.TabIndex = 90;
            this.rtxtChat.Text = "";
            // 
            // btnSetPos
            // 
            this.btnSetPos.Location = new System.Drawing.Point(943, 313);
            this.btnSetPos.Name = "btnSetPos";
            this.btnSetPos.Size = new System.Drawing.Size(100, 23);
            this.btnSetPos.TabIndex = 108;
            this.btnSetPos.Text = "Set Position";
            this.btnSetPos.UseVisualStyleBackColor = true;
            this.btnSetPos.Click += new System.EventHandler(this.btnSetPos_Click);
            // 
            // cboxSearchDepth
            // 
            this.cboxSearchDepth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxSearchDepth.FormattingEnabled = true;
            this.cboxSearchDepth.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.cboxSearchDepth.Location = new System.Drawing.Point(950, 283);
            this.cboxSearchDepth.Name = "cboxSearchDepth";
            this.cboxSearchDepth.Size = new System.Drawing.Size(81, 21);
            this.cboxSearchDepth.TabIndex = 109;
            // 
            // pnlRules
            // 
            this.pnlRules.Controls.Add(this.btnSwap);
            this.pnlRules.Controls.Add(this.checkMoveback);
            this.pnlRules.Controls.Add(this.radioTime60);
            this.pnlRules.Controls.Add(this.radioTime30);
            this.pnlRules.Controls.Add(this.radioTime20);
            this.pnlRules.Controls.Add(this.radioTime15);
            this.pnlRules.Controls.Add(this.radioTime10);
            this.pnlRules.Controls.Add(this.radioTime5);
            this.pnlRules.Controls.Add(this.radioTime3);
            this.pnlRules.Enabled = false;
            this.pnlRules.Location = new System.Drawing.Point(921, 34);
            this.pnlRules.Name = "pnlRules";
            this.pnlRules.Size = new System.Drawing.Size(144, 216);
            this.pnlRules.TabIndex = 110;
            // 
            // btnSwap
            // 
            this.btnSwap.Location = new System.Drawing.Point(19, 157);
            this.btnSwap.Name = "btnSwap";
            this.btnSwap.Size = new System.Drawing.Size(100, 23);
            this.btnSwap.TabIndex = 102;
            this.btnSwap.Text = "Swap colors";
            this.btnSwap.UseVisualStyleBackColor = true;
            // 
            // checkMoveback
            // 
            this.checkMoveback.AutoSize = true;
            this.checkMoveback.Location = new System.Drawing.Point(5, 192);
            this.checkMoveback.Name = "checkMoveback";
            this.checkMoveback.Size = new System.Drawing.Size(135, 17);
            this.checkMoveback.TabIndex = 110;
            this.checkMoveback.Text = "Taking moves back off";
            this.checkMoveback.UseVisualStyleBackColor = true;
            // 
            // radioTime60
            // 
            this.radioTime60.AutoSize = true;
            this.radioTime60.Location = new System.Drawing.Point(31, 127);
            this.radioTime60.Name = "radioTime60";
            this.radioTime60.Size = new System.Drawing.Size(76, 17);
            this.radioTime60.TabIndex = 109;
            this.radioTime60.TabStop = true;
            this.radioTime60.Tag = "3600";
            this.radioTime60.Text = "60 minutes";
            this.radioTime60.UseVisualStyleBackColor = true;
            // 
            // radioTime30
            // 
            this.radioTime30.AutoSize = true;
            this.radioTime30.Location = new System.Drawing.Point(31, 107);
            this.radioTime30.Name = "radioTime30";
            this.radioTime30.Size = new System.Drawing.Size(76, 17);
            this.radioTime30.TabIndex = 108;
            this.radioTime30.TabStop = true;
            this.radioTime30.Tag = "1800";
            this.radioTime30.Text = "30 minutes";
            this.radioTime30.UseVisualStyleBackColor = true;
            // 
            // radioTime20
            // 
            this.radioTime20.AutoSize = true;
            this.radioTime20.Location = new System.Drawing.Point(31, 87);
            this.radioTime20.Name = "radioTime20";
            this.radioTime20.Size = new System.Drawing.Size(76, 17);
            this.radioTime20.TabIndex = 107;
            this.radioTime20.TabStop = true;
            this.radioTime20.Tag = "1200";
            this.radioTime20.Text = "20 minutes";
            this.radioTime20.UseVisualStyleBackColor = true;
            // 
            // radioTime15
            // 
            this.radioTime15.AutoSize = true;
            this.radioTime15.Location = new System.Drawing.Point(31, 67);
            this.radioTime15.Name = "radioTime15";
            this.radioTime15.Size = new System.Drawing.Size(76, 17);
            this.radioTime15.TabIndex = 106;
            this.radioTime15.TabStop = true;
            this.radioTime15.Tag = "900";
            this.radioTime15.Text = "15 minutes";
            this.radioTime15.UseVisualStyleBackColor = true;
            // 
            // radioTime10
            // 
            this.radioTime10.AutoSize = true;
            this.radioTime10.Location = new System.Drawing.Point(31, 47);
            this.radioTime10.Name = "radioTime10";
            this.radioTime10.Size = new System.Drawing.Size(76, 17);
            this.radioTime10.TabIndex = 105;
            this.radioTime10.TabStop = true;
            this.radioTime10.Tag = "600";
            this.radioTime10.Text = "10 minutes";
            this.radioTime10.UseVisualStyleBackColor = true;
            // 
            // radioTime5
            // 
            this.radioTime5.AutoSize = true;
            this.radioTime5.Location = new System.Drawing.Point(31, 27);
            this.radioTime5.Name = "radioTime5";
            this.radioTime5.Size = new System.Drawing.Size(70, 17);
            this.radioTime5.TabIndex = 104;
            this.radioTime5.TabStop = true;
            this.radioTime5.Tag = "300";
            this.radioTime5.Text = "5 minutes";
            this.radioTime5.UseVisualStyleBackColor = true;
            // 
            // radioTime3
            // 
            this.radioTime3.AutoSize = true;
            this.radioTime3.Location = new System.Drawing.Point(31, 7);
            this.radioTime3.Name = "radioTime3";
            this.radioTime3.Size = new System.Drawing.Size(70, 17);
            this.radioTime3.TabIndex = 103;
            this.radioTime3.TabStop = true;
            this.radioTime3.Tag = "180";
            this.radioTime3.Text = "3 minutes";
            this.radioTime3.UseVisualStyleBackColor = true;
            // 
            // pnlMode
            // 
            this.pnlMode.Controls.Add(this.lblAI);
            this.pnlMode.Controls.Add(this.lblOR2);
            this.pnlMode.Controls.Add(this.btnAI);
            this.pnlMode.Controls.Add(this.btnCreate);
            this.pnlMode.Controls.Add(this.btnJoin);
            this.pnlMode.Controls.Add(this.lblServer);
            this.pnlMode.Controls.Add(this.lblClient);
            this.pnlMode.Controls.Add(this.lblIP);
            this.pnlMode.Controls.Add(this.lblOR);
            this.pnlMode.Controls.Add(this.txtIP);
            this.pnlMode.Enabled = false;
            this.pnlMode.Location = new System.Drawing.Point(745, 86);
            this.pnlMode.Name = "pnlMode";
            this.pnlMode.Size = new System.Drawing.Size(146, 286);
            this.pnlMode.TabIndex = 111;
            // 
            // lblAI
            // 
            this.lblAI.Location = new System.Drawing.Point(9, 252);
            this.lblAI.Name = "lblAI";
            this.lblAI.Size = new System.Drawing.Size(125, 26);
            this.lblAI.TabIndex = 117;
            this.lblAI.Text = "Click the button to play with the computer.";
            this.lblAI.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblOR2
            // 
            this.lblOR2.Location = new System.Drawing.Point(22, 203);
            this.lblOR2.Name = "lblOR2";
            this.lblOR2.Size = new System.Drawing.Size(100, 23);
            this.lblOR2.TabIndex = 116;
            this.lblOR2.Text = "OR";
            this.lblOR2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnAI
            // 
            this.btnAI.Location = new System.Drawing.Point(14, 228);
            this.btnAI.Name = "btnAI";
            this.btnAI.Size = new System.Drawing.Size(114, 23);
            this.btnAI.TabIndex = 115;
            this.btnAI.Text = "Play with AI";
            this.btnAI.UseVisualStyleBackColor = true;
            this.btnAI.Click += new System.EventHandler(this.btnAI_Click);
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(15, 9);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(114, 23);
            this.btnCreate.TabIndex = 108;
            this.btnCreate.Text = "Create new game";
            this.btnCreate.UseVisualStyleBackColor = true;
            // 
            // btnJoin
            // 
            this.btnJoin.Location = new System.Drawing.Point(15, 153);
            this.btnJoin.Name = "btnJoin";
            this.btnJoin.Size = new System.Drawing.Size(114, 23);
            this.btnJoin.TabIndex = 109;
            this.btnJoin.Text = "Join game";
            this.btnJoin.UseVisualStyleBackColor = true;
            // 
            // lblServer
            // 
            this.lblServer.Location = new System.Drawing.Point(14, 33);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(117, 30);
            this.lblServer.TabIndex = 110;
            this.lblServer.Text = "Click the button to create a server.";
            this.lblServer.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblClient
            // 
            this.lblClient.Location = new System.Drawing.Point(3, 178);
            this.lblClient.Name = "lblClient";
            this.lblClient.Size = new System.Drawing.Size(141, 25);
            this.lblClient.TabIndex = 113;
            this.lblClient.Text = "and click the button to join.";
            this.lblClient.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblIP
            // 
            this.lblIP.Location = new System.Drawing.Point(16, 97);
            this.lblIP.Name = "lblIP";
            this.lblIP.Size = new System.Drawing.Size(113, 30);
            this.lblIP.TabIndex = 112;
            this.lblIP.Text = "Enter the IP address to connect to:";
            this.lblIP.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblOR
            // 
            this.lblOR.Location = new System.Drawing.Point(22, 72);
            this.lblOR.Name = "lblOR";
            this.lblOR.Size = new System.Drawing.Size(100, 23);
            this.lblOR.TabIndex = 114;
            this.lblOR.Text = "OR";
            this.lblOR.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtIP
            // 
            this.txtIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtIP.Location = new System.Drawing.Point(15, 127);
            this.txtIP.Name = "txtIP";
            this.txtIP.Size = new System.Drawing.Size(114, 20);
            this.txtIP.TabIndex = 111;
            this.txtIP.Text = "127.0.0.1";
            this.txtIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // CFMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1074, 766);
            this.Controls.Add(this.cboxSearchDepth);
            this.Controls.Add(this.btnSetPos);
            this.Controls.Add(this.pnlNotation);
            this.Controls.Add(this.btnReady);
            this.Controls.Add(this.btnDraw);
            this.Controls.Add(this.btnMoveback);
            this.Controls.Add(this.btnResign);
            this.Controls.Add(this.lblClockBlack);
            this.Controls.Add(this.lblClockWhite);
            this.Controls.Add(this.lblReady);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblColon);
            this.Controls.Add(this.lblBlackScore);
            this.Controls.Add(this.lblWhiteScore);
            this.Controls.Add(this.lblBlackName);
            this.Controls.Add(this.lblWhiteName);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtMessage);
            this.Controls.Add(this.rtxtChat);
            this.Controls.Add(this.pboxBlack);
            this.Controls.Add(this.pboxWhite);
            this.Controls.Add(this.pboxFrameWhite);
            this.Controls.Add(this.pboxFrameBlack);
            this.Controls.Add(this.pnlRules);
            this.Controls.Add(this.pnlMode);
            this.MaximumSize = new System.Drawing.Size(1090, 800);
            this.MinimumSize = new System.Drawing.Size(1090, 800);
            this.Name = "CFMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chess v1.0 by Kuba";
            this.pnlNotation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pboxWhite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxBlack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxFrameWhite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxFrameBlack)).EndInit();
            this.pnlRules.ResumeLayout(false);
            this.pnlRules.PerformLayout();
            this.pnlMode.ResumeLayout(false);
            this.pnlMode.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion        

        private System.Windows.Forms.Panel pnlNotation;
        private System.Windows.Forms.Panel pnlChessboard;
        private System.Windows.Forms.Button btnMoveback;
        private System.Windows.Forms.Button btnResign;
        private System.Windows.Forms.Button btnDraw;
        private System.Windows.Forms.Button btnReady;

        private System.Windows.Forms.TextBox txtName;    
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.RichTextBox rtxtChat;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblWhiteName;
        private System.Windows.Forms.Label lblBlackName;
        private System.Windows.Forms.Label lblReady;
        private System.Windows.Forms.Label lblClockWhite;
        private System.Windows.Forms.Label lblClockBlack;
        private System.Windows.Forms.Label lblWhiteScore;
        private System.Windows.Forms.Label lblBlackScore;
        private System.Windows.Forms.Label lblColon;

        private System.Windows.Forms.PictureBox pboxWhite;
        private System.Windows.Forms.PictureBox pboxBlack;
        private System.Windows.Forms.PictureBox pboxFrameWhite;
        private System.Windows.Forms.PictureBox pboxFrameBlack;
        private System.Windows.Forms.Button btnSetPos;
        private System.Windows.Forms.ComboBox cboxSearchDepth;
        private System.Windows.Forms.Panel pnlRules;
        private System.Windows.Forms.Button btnSwap;
        private System.Windows.Forms.CheckBox checkMoveback;
        private System.Windows.Forms.RadioButton radioTime60;
        private System.Windows.Forms.RadioButton radioTime30;
        private System.Windows.Forms.RadioButton radioTime20;
        private System.Windows.Forms.RadioButton radioTime15;
        private System.Windows.Forms.RadioButton radioTime10;
        private System.Windows.Forms.RadioButton radioTime5;
        private System.Windows.Forms.RadioButton radioTime3;
        private System.Windows.Forms.Panel pnlMode;
        private System.Windows.Forms.Label lblAI;
        private System.Windows.Forms.Label lblOR2;
        private System.Windows.Forms.Button btnAI;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Button btnJoin;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.Label lblClient;
        private System.Windows.Forms.Label lblIP;
        private System.Windows.Forms.Label lblOR;
        private System.Windows.Forms.TextBox txtIP;
    }
}

