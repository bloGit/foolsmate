﻿/*
 this is not a real application, this is a minimal GUI on behalf of testing foolsmate engine;
*/

using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System;

namespace foolGUI
{
    class Const
    {        
        public const string DLLPATH = "foolsmate.dll";

        public static readonly ComponentResourceManager RES_MANAGER = new ComponentResourceManager(typeof(CFMain));

        public static readonly string[,] IMG_NAMES = {
            {"white_pawn",  "white_knight",  "white_bishop",  "white_rook",  "white_queen",  "white_king"},
            {"black_pawn",  "black_knight",  "black_bishop",  "black_rook",  "black_queen",  "black_king"},
            {"orange_pawn", "orange_knight", "orange_bishop", "orange_rook", "orange_queen", "orange_king"} };

        public static readonly Color DARK = Color.FromArgb(195, 125, 20);
        public static readonly Color BRIGHT = Color.FromArgb(225, 175, 90);
        public static readonly Color MIDDLE = Color.FromArgb(215, 153, 60);
        public static readonly Color BORDER = Color.FromArgb(120, 50, 0);
        public static readonly Color LASTMOVE = Color.FromArgb(180, 150, 110);
        public static readonly Color BAR = Color.FromArgb(140, 90, 00); //Position Setter pieces bars
    }


    enum EType { PAWN, KNIGHT, BISHOP, ROOK, QUEEN, KING, NONE };
    enum EColor { WHITE, BLACK };    

    class CSquare : PictureBox
    {
        private int m_idx;        
        private Color m_origColor;
        private PictureBox m_border = null;
        private const int BORDER_WIDTH = 2;
        private bool m_lastMove = false;
        private Image m_ghostImage = null;
        private ulong m_legalMoves = 0;
        public EType m_pieceType;   //used by CPositionSetter
        public EColor m_pieceColor; //used by CPositionSetter

        [DllImport(Const.DLLPATH)][return: MarshalAs(UnmanagedType.I1)]
        private static extern bool foolsmate_movePiece(int a_from, int a_to);

        public CSquare(int a_idx, int a_size, int a_x, int a_y, ControlCollection a_controls)
        {            
            m_idx = a_idx;
            int x = m_idx & 7; int y = m_idx >> 3; //for some calculation purpose convert the scalar index to x,y coords

            int size = a_size - BORDER_WIDTH * 2; //shrink the square actual size to make room for the border around it
            Size = new Size(size, size);
            Location = new Point(a_x + x*a_size, a_y + y*-a_size);
            AllowDrop = true;

            m_border = new PictureBox();
            m_border.Size = new Size(a_size, a_size);  
            m_border.AllowDrop = true;
            m_border.Location = new Point(Location.X - BORDER_WIDTH, Location.Y - BORDER_WIDTH); //offset border in order to center it against square

            if (x % 2 == 0 && y % 2 == 0 || x % 2 == 1 && y % 2 == 1)
            {
                m_origColor = Const.DARK;
            }
            else
            {
                m_origColor = Const.BRIGHT;
            }
            setOriginalColor();
            a_controls.Add(this);            
            a_controls.Add(m_border);
        }

        public void addBorderEventHandlers(Action<object, EventArgs> a_dragDrop, Action<object, DragEventArgs> a_dragEnter)
        {
            m_border.DragDrop  += new DragEventHandler(a_dragDrop); //ensure DragDrop detection on edges
            m_border.DragEnter += new DragEventHandler(a_dragEnter);
        }

        public void mouseEnter(CSquare a_src)
        {
            if (a_src == null && m_legalMoves != 0 ||
                a_src != null && ((ulong)1 << m_idx & a_src.m_legalMoves) != 0)
            {
                BackColor = Const.MIDDLE;
                m_border.BackColor = Const.BORDER;
                Cursor = Cursors.Hand;
            }
        }

        public void mouseLeave()
        {            
            if (m_lastMove)
            {
                setLastMoveColor();
            }
            else
            {
                setOriginalColor();
            }
            Cursor = Cursors.Default;
        }

        public bool pickUpPiece()
        {
            if (m_legalMoves != 0)
            {
                mouseLeave();
                return true;
            }
            return false;
        }

        public bool putPiece(CSquare a_src)
        {
            if (foolsmate_movePiece(a_src.m_idx, m_idx))
            {
                mouseLeave();
                return true;
            }
            return false;            
        }

        public void setOriginalColor()
        {
            BackColor = m_border.BackColor = m_origColor;
            m_lastMove = false;            
        }

        public void setLastMoveColor()
        {
            BackColor = Const.LASTMOVE;
            m_border.BackColor = Const.MIDDLE;
            m_lastMove = true;
        }

        public void switchToGhost()
        {
            Image = m_ghostImage;
        }

        public Image Ghost
        {
            set { m_ghostImage = value; }
        }

        public ulong Moves
        {
            set { m_legalMoves = value; }
        }

        public void setSquare(EType a_type, EColor a_color) //used by CPositionSetter
        {
            m_pieceType = a_type;
            m_pieceColor = a_color;            
        }
    };
}
