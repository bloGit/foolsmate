﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Xml.Linq;
using System.IO;

namespace foolGUI
{

public partial class CFPositionSetter : Form
{
    //********** fields: **********

    private const int SQUARE_SIZE = 40;
    private CSquare[] m_chessboard = new CSquare[64];
    private CSquare[,] m_piecesBars = new CSquare[6, 2];
    private CSquare m_srcSquare = null;    
    private CSquare m_barSquare = null;    
    private Image m_cursorImage = null;
    private bool m_pickedUp = false;
    private Cursor m_cursor;
    private XDocument m_layoutContainer = new XDocument();
    private const string XML_ROOT = "layoutContainer";
    private const string XML_PATH = "layoutContainer.xml";
    

    //********** methods: **********
    
    [DllImport(Const.DLLPATH)]
    private static extern void foolsmate_setCustomPosition(IntPtr unmanaged_ptr, EColor color);

    public CFPositionSetter()
    {
        InitializeComponent();        
        drawPiecesBars();
        drawChessboard();
        handleXMLLayouts();        
    }

    private void drawPiecesBars()
    {
        Size size = new Size(SQUARE_SIZE-4, SQUARE_SIZE-4);
        const int X = 60;
        const int Y = 420;
        for (int i = 0; i < 6; i++)
        {
            foreach (int j in new List<int>{0, 1})
            {
                m_piecesBars[i, j] = new CSquare(i+j*80, SQUARE_SIZE, X, Y, Controls); //i+j*80: the trick places squares in two distant rows ('bars') around the chessboard model
                m_piecesBars[i, j].Image = new Bitmap( (Image)Const.RES_MANAGER.GetObject(Const.IMG_NAMES[j, i]), size );
                m_piecesBars[i, j].BackColor = Const.BAR;
                m_piecesBars[i, j].GiveFeedback += new GiveFeedbackEventHandler(square_GiveFeedback);
                m_piecesBars[i, j].DragDrop += new DragEventHandler(barSquare_DragDrop);
                m_piecesBars[i, j].DragEnter += new DragEventHandler(barSquare_DragEnter);                
                m_piecesBars[i, j].MouseDown += new MouseEventHandler(barSquare_MouseDown);                
                m_piecesBars[i, j].MouseMove += new MouseEventHandler(barSquare_MouseMove);
                m_piecesBars[i, j].MouseEnter += new EventHandler(square_MouseEnter);
                m_piecesBars[i, j].MouseLeave += new EventHandler(barSquare_MouseLeave);
                m_piecesBars[i, j].setSquare((EType)i, (EColor)j);                
            }
        }
    }

    private void drawChessboard()
    {
        const int X = 20;
        const int Y = 360;        
        for (int i = 0; i < 64; i++)
        {
            m_chessboard[i] = new CSquare(i, SQUARE_SIZE, X, Y, Controls);
            m_chessboard[i].GiveFeedback += new GiveFeedbackEventHandler(square_GiveFeedback);
            m_chessboard[i].DragDrop += new DragEventHandler(square_DragDrop);
            m_chessboard[i].DragEnter += new DragEventHandler(square_DragEnter);
            m_chessboard[i].DragLeave += new EventHandler(square_DragLeave);
            m_chessboard[i].MouseDown += new MouseEventHandler(square_MouseDown);                
            m_chessboard[i].MouseMove += new MouseEventHandler(square_MouseMove);
            m_chessboard[i].MouseEnter += new EventHandler(square_MouseEnter);
            m_chessboard[i].MouseLeave += new EventHandler(square_MouseLeave);
            m_chessboard[i].Click += new EventHandler(square_RightClick);            
        }
        btnClear_Click(null, null);
    }

    private void handleXMLLayouts()
    {        
        if (File.Exists(XML_PATH))
        {
            m_layoutContainer = XDocument.Load(XML_PATH);
            foreach (var item in m_layoutContainer.Element(XML_ROOT).Elements())
            {
                cboxLayouts.Items.Add(item.Element("name").Value);
            }
        }
        else
        {
            m_layoutContainer.Add(new XElement(XML_ROOT));
        }
    }

    private void barSquare_MouseLeave(object sender, EventArgs e) //MouseEnter for bar squares is square_MouseEnter
    {        
        ((CSquare)sender).BackColor = Const.BAR;
        ((CSquare)sender).Cursor = Cursors.Default;
    }

    [DllImportAttribute("user32.dll", CharSet = CharSet.Auto)] static extern bool DestroyIcon(IntPtr handle); //for DestroyIcon() Win32    
    private void barSquare_MouseDown(object sender, EventArgs e)
    {
        m_srcSquare = null;
        m_barSquare = (CSquare)sender;
        barSquare_MouseLeave(m_barSquare, null);
        m_cursorImage = m_barSquare.Image;
        m_cursor = new Cursor(((Bitmap)m_cursorImage).GetHicon());
        Cursor.Current = m_cursor;
        DestroyIcon(m_cursor.Handle);  //protects from 'generic error in GDI+' exception          
        m_pickedUp = true;        
    }

    private void barSquare_DragDrop(object sender, EventArgs e)
    {                
        m_pickedUp = false;
    }

    private void barSquare_DragEnter(object sender, DragEventArgs e)
    {        
        e.Effect = DragDropEffects.Move;
    }

    private void barSquare_MouseMove(object sender, EventArgs e)
    {
        if (m_pickedUp)
        {
            m_barSquare.DoDragDrop(0, DragDropEffects.Move);
        }
    }

    private void square_MouseEnter(object sender, EventArgs e)
    {        
        if ( ((CSquare)sender).Image != null )
        {
            ((CSquare)sender).BackColor = Const.MIDDLE;
            ((CSquare)sender).Cursor = Cursors.Hand;
        }
    }

    private void square_MouseLeave(object sender, EventArgs e)
    {        
        ((CSquare)sender).mouseLeave();
    }

    private void square_MouseDown(object sender, MouseEventArgs e)
    {
        m_srcSquare = (CSquare)sender;
        if (e.Button == MouseButtons.Left  &&  m_srcSquare.Image != null)
        {
            square_MouseLeave(m_srcSquare, null);
            m_cursorImage = m_srcSquare.Image;
            m_cursor = new Cursor(((Bitmap)m_cursorImage).GetHicon());
            Cursor.Current = m_cursor;
            DestroyIcon(m_cursor.Handle);  //protects from 'generic error in GDI+' exception          
            m_pickedUp = true;
        }
    }

    private void square_GiveFeedback(object sender, GiveFeedbackEventArgs e)
    {
        e.UseDefaultCursors = false;
        m_cursor = new Cursor(((Bitmap)m_cursorImage).GetHicon());
        Cursor.Current = m_cursor;
        DestroyIcon(m_cursor.Handle);  //protects from 'generic error in GDI+' exception
    }

    private void square_DragDrop(object sender, EventArgs e)
    {
        var square = (CSquare)sender;
        square.Image = m_cursorImage;
        if (m_srcSquare != null  &&  m_srcSquare != square) //Image comes from chessboard square
        {
            square.m_pieceType = m_srcSquare.m_pieceType;
            square.m_pieceColor = m_srcSquare.m_pieceColor;
            m_srcSquare.m_pieceType = EType.NONE;
            m_srcSquare.Image = null;
        }
        else //Image comes from bar square
        {
            square.m_pieceType = m_barSquare.m_pieceType;
            square.m_pieceColor = m_barSquare.m_pieceColor;
        }
        m_pickedUp = false;
    }

    private void square_DragEnter(object sender, DragEventArgs e)
    {        
        ((CSquare)sender).BackColor = Const.MIDDLE;
        e.Effect = DragDropEffects.Move;
    }

    private void square_DragLeave(object sender, EventArgs e)
    {
        ((CSquare)sender).mouseLeave();
    }

    private void square_MouseMove(object sender, EventArgs e)
    {
        if (m_pickedUp && m_srcSquare != null)
        {
            m_srcSquare.DoDragDrop(0, DragDropEffects.Move);
        }
    }

    private void square_RightClick(object sender, EventArgs e)
    {        
        if ( ((MouseEventArgs)e).Button == MouseButtons.Right )
        {
            ((CSquare)sender).m_pieceType = EType.NONE;
            ((CSquare)sender).Image = null;
            ((CSquare)sender).mouseLeave();
        }
    }

    private void btnClear_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < 64; i++)
        {
            m_chessboard[i].m_pieceType = EType.NONE;
            m_chessboard[i].Image = null;                         
        }    
    }

    private void btnDefault_Click(object sender, EventArgs e)
    {
        btnClear_Click(sender, e);
        m_chessboard[4].setSquare(EType.KING,   EColor.WHITE);
        m_chessboard[3].setSquare(EType.QUEEN,  EColor.WHITE);
        m_chessboard[0].setSquare(EType.ROOK,   EColor.WHITE);
        m_chessboard[7].setSquare(EType.ROOK,   EColor.WHITE);
        m_chessboard[2].setSquare(EType.BISHOP, EColor.WHITE);
        m_chessboard[5].setSquare(EType.BISHOP, EColor.WHITE);
        m_chessboard[1].setSquare(EType.KNIGHT, EColor.WHITE);
        m_chessboard[6].setSquare(EType.KNIGHT, EColor.WHITE);
        for (int i = 8; i < 16; i++)
        {
            m_chessboard[i].setSquare(EType.PAWN, EColor.WHITE);
        }
        m_chessboard[60].setSquare(EType.KING,   EColor.BLACK);
        m_chessboard[59].setSquare(EType.QUEEN,  EColor.BLACK);
        m_chessboard[56].setSquare(EType.ROOK,   EColor.BLACK);
        m_chessboard[63].setSquare(EType.ROOK,   EColor.BLACK);
        m_chessboard[58].setSquare(EType.BISHOP, EColor.BLACK);
        m_chessboard[61].setSquare(EType.BISHOP, EColor.BLACK);
        m_chessboard[57].setSquare(EType.KNIGHT, EColor.BLACK);
        m_chessboard[62].setSquare(EType.KNIGHT, EColor.BLACK);
        for (int i = 48; i < 56; i++)
        {
            m_chessboard[i].setSquare(EType.PAWN, EColor.BLACK);
        }

        for (int i = 0; i < 64; i++)
        {
            EType type = m_chessboard[i].m_pieceType;
            EColor color = m_chessboard[i].m_pieceColor;
            if (type != EType.NONE)
            {
                m_chessboard[i].Image = m_piecesBars[(int)type, (int)color].Image;
            }            
        }
    }

    private void btnGenPos_Click(object sender, EventArgs e)
    {
        //serialize the chessboard layout
        byte[] serializedArray = new byte[8*8*2];        
        for (int i = 0, k = 0; i < 64; i++)
        {
            serializedArray[k++] = (byte)m_chessboard[i].m_pieceType;
            serializedArray[k++] = (byte)m_chessboard[i].m_pieceColor;         
        }    
             
        //send it to API   
        int size = Marshal.SizeOf(serializedArray[0]) * serializedArray.Length;
        IntPtr unmanaged_ptr = Marshal.AllocHGlobal(size);
        Marshal.Copy(serializedArray, 0, unmanaged_ptr, serializedArray.Length);
        foolsmate_setCustomPosition(unmanaged_ptr, rbtn_white.Checked ? EColor.WHITE : EColor.BLACK);
        Marshal.FreeHGlobal(unmanaged_ptr);
        Hide();
    }

    private void btnSave_Click(object sender, EventArgs e)
    {
        var layout = new XElement("layout");
        var name = new XElement("name", txtName.Text); //prevent saving the same name twice!!
        layout.Add(name);
        for (int i = 0; i < 64; i++)
        {                                        
            var type = new XElement("type", m_chessboard[i].m_pieceType); 
            var color = new XElement("color", m_chessboard[i].m_pieceColor);
            var square = new XElement("square");
            square.Add(type);
            square.Add(color);
            layout.Add(square);            
        }
        m_layoutContainer.Element(XML_ROOT).Add(layout);
        m_layoutContainer.Save(XML_PATH);
        cboxLayouts.Items.Add(layout.Element("name").Value);
    }

    private void cboxLayouts_TextChanged(object sender, EventArgs e)
    {
        List<string> list = (
            from layout in m_layoutContainer.Element(XML_ROOT).Elements()
            where layout.Element("name").Value == cboxLayouts.Text            
            from square in layout.Elements()
            where square.Name == "square"
            from item in square.Elements()
            select item.Value ).ToList();
                
        EType type;
        EColor color;
        for (int i = 0, k = 0; i < 64; i++)
        {
            Enum.TryParse(list[k++], out type);
            Enum.TryParse(list[k++], out color);
            m_chessboard[i].m_pieceType = type;
            m_chessboard[i].m_pieceColor = color;
            m_chessboard[i].Image = null;
            if (type != EType.NONE)
            {
                m_chessboard[i].Image = m_piecesBars[(int)type, (int)color].Image;
            }            
        }
    }

    private void btnDelete_Click(object sender, EventArgs e)
    {
        m_layoutContainer.Element(XML_ROOT).Elements()
                         .Where(layout => layout.Element("name").Value == cboxLayouts.Text).Remove();
        m_layoutContainer.Save(XML_PATH);
        cboxLayouts.Items.Remove(cboxLayouts.Text);
    }
}

} //namespace foolGUI
