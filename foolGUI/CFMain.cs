﻿/*
 this is not a real application, this is a minimal GUI on behalf of testing foolsmate engine;
*/

using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;

namespace foolGUI
{

public partial class CFMain: Form
{
    //********** fields: **********

    private CFPositionSetter m_positionSetter = null;

    private const int SQUARE_SIZE = 80;
    private CSquare[] m_chessboard = new CSquare[64];    
    private CSquare m_srcSquare = null;    
        
    private Image[,] m_images = new Image[3, 6];
    private Image m_cursorImage = null;            

    private bool m_myTurn = true;    

    Cursor m_cursor;

    private delegate void handleRedrawDelegate(IntPtr serializedPattern);
    private handleRedrawDelegate m_redrawDelegate;
    
    private Thread m_AIThread;

    //System.Timers.Timer m_clock = new System.Timers.Timer();
    System.Diagnostics.Stopwatch m_stopwatch = new System.Diagnostics.Stopwatch();
    
    //********** methods: **********

    [DllImport(Const.DLLPATH)]
    private static extern void foolsmate_registerCallback_redraw(handleRedrawDelegate del);

    [DllImport(Const.DLLPATH)]
    private static extern void foolsmate_createGame();

    [DllImport(Const.DLLPATH)]
    private static extern void foolsmate_resetGame();

    [DllImport(Const.DLLPATH)]
    private static extern void foolsmate_takeBackMove();

    [DllImport(Const.DLLPATH)]
    private static extern void foolsmate_makeBestMovement(int a_searchDepth);

    public CFMain()
    {
        InitializeComponent();
        init();
    }

    private void loadImagesFromResx()
    {        
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                m_images[i, j] = (Image)Const.RES_MANAGER.GetObject(Const.IMG_NAMES[i, j]);
            }
        }
    }

    void init()
    {
        loadImagesFromResx();
        drawChessboard();
        
        foolsmate_createGame();

        //create two delegates and pass them to foolsmate engine to register callbacks
        m_redrawDelegate = new handleRedrawDelegate(handleRedrawCallback);
        foolsmate_registerCallback_redraw(m_redrawDelegate);

        btnMoveback.Enabled = true; //TEMPORARY HERE
        cboxSearchDepth.SelectedIndex = 8;
        
        lblName.Enabled = true;  //enable only Name section
        txtName.Enabled = true;
        txtName.Select();        
    }

    private void drawChessboard()
    {
        const int X = 9;
        const int Y = 569;        
        for (int i = 0; i < 64; i++)
        {
            m_chessboard[i] = new CSquare(i, SQUARE_SIZE, X, Y, pnlChessboard.Controls);
            m_chessboard[i].GiveFeedback += new GiveFeedbackEventHandler(square_GiveFeedback);
            m_chessboard[i].DragDrop += new DragEventHandler(square_DragDrop);
            m_chessboard[i].DragEnter += new DragEventHandler(square_DragEnter);
            m_chessboard[i].DragLeave += new EventHandler(square_DragLeave);
            m_chessboard[i].MouseDown += new MouseEventHandler(square_MouseDown);
            m_chessboard[i].MouseMove += new MouseEventHandler(square_MouseMove);
            m_chessboard[i].MouseEnter += new EventHandler(square_MouseEnter);
            m_chessboard[i].MouseLeave += new EventHandler(square_MouseLeave);
            m_chessboard[i].addBorderEventHandlers(border_DragDrop, border_DragEnter);            
        }
        drawBoardNotation(58, 1, true);
        drawBoardNotation(58, 1, false);        
    }       

    private void drawBoardNotation(int a_x, int a_y, bool a_files)
    {                    
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                Label lbl = new Label();                
                int x = a_x + j * SQUARE_SIZE;
                int y = a_y + i * 672; //672 - distance between two corresponding rows
                if (!a_files) {int z = x; x = y; y = z;} //swap x <-> y for ranks drawing
                lbl.Location = new Point(x, y);
                lbl.Text = ((char)(a_files ? 'A'+j : '8'-j)).ToString();
                lbl.Font = new Font("Microsoft Sans Serif", 9F, FontStyle.Bold);
                lbl.ForeColor = Const.BRIGHT;
                lbl.Size = new Size(15, 15);
                pnlNotation.Controls.Add(lbl);
            }
        }             
    }

    private void handleRedrawCallback(IntPtr serializedPattern)
    {
        byte[] pattern = new byte[64 * 16];
        Marshal.Copy(serializedPattern, pattern, 0, 64 * 16);
                
        for (int i = 0, k = 0; i < 64; i++, k+=16)
        {
            m_chessboard[i].Moves = BitConverter.ToUInt64(pattern, k);
            if (pattern[k+8] != (int)EType.NONE)
            {
                int type = pattern[k+8];
                int color = pattern[k+9];
                if (m_chessboard[i].Image != m_images[color, type])
                {
                    m_chessboard[i].Image = m_images[color, type];
                    m_chessboard[i].Ghost = m_images[2, type];
                }
            }
            else
            {
                m_chessboard[i].Image = null;
            }

            if (pattern[k+10] == 1)
            {
                m_chessboard[i].setLastMoveColor();
            }
            else
            {
                m_chessboard[i].setOriginalColor();
            }            
        }
        if (m_myTurn)
        {
            m_stopwatch.Stop();
            Console.WriteLine("Elapsed = {0}", m_stopwatch.Elapsed);
            m_stopwatch.Reset();            
        }
    }

    private void square_MouseEnter(object sender, EventArgs e)
    {
        if (m_myTurn)
        {
            CSquare square = (CSquare)sender;
            square.mouseEnter(m_srcSquare);
        }
    }

    private void square_MouseLeave(object sender, EventArgs e)
    {
        ((CSquare)sender).mouseLeave();
    }

    [DllImportAttribute("user32.dll", CharSet = CharSet.Auto)] static extern bool DestroyIcon(IntPtr handle); //for DestroyIcon() Win32
    private void square_MouseDown(object sender, EventArgs e)
    {
        if (m_myTurn && ((CSquare)sender).pickUpPiece())
        {
            m_srcSquare = (CSquare)sender;
            m_cursorImage = m_srcSquare.Image;
            m_srcSquare.switchToGhost();
            m_cursor = new Cursor( ((Bitmap)m_cursorImage).GetHicon() );
            Cursor.Current = m_cursor;
            DestroyIcon(m_cursor.Handle);  //protects from 'generic error in GDI+' exception
        }
    }

    private void square_GiveFeedback(object sender, GiveFeedbackEventArgs e)
    {
        e.UseDefaultCursors = false;
        m_cursor = new Cursor( ((Bitmap)m_cursorImage).GetHicon() );
        Cursor.Current = m_cursor;
        DestroyIcon(m_cursor.Handle);  //protects from 'generic error in GDI+' exception
    }

    private void square_DragDrop(object sender, EventArgs e)
    {
        CSquare square = (CSquare)sender;
        if (square.putPiece(m_srcSquare))
        {                                            
            //m_myTurn = false;            
            int depth = cboxSearchDepth.SelectedIndex + 1;
            m_stopwatch.Start();
            m_AIThread = new Thread(() => foolsmate_makeBestMovement(depth));
            m_AIThread.Start();                                                
        }    
        else
        {        
            m_srcSquare.Image = m_cursorImage;        
        }
        m_srcSquare = null;
    }        

    private void square_DragEnter(object sender, DragEventArgs e)
    {
        square_MouseEnter(sender, e);
        e.Effect = DragDropEffects.Move;
    }

    private void square_DragLeave(object sender, EventArgs e)
    {
        ((CSquare)sender).mouseLeave();
    }

    private void square_MouseMove(object sender, EventArgs e)
    {
        if (m_srcSquare != null)
        {                
            m_srcSquare.DoDragDrop(0, DragDropEffects.Move);
        }
    }

    private void border_DragDrop(object sender, EventArgs e)
    { 
        m_srcSquare.Image = m_cursorImage;                
        m_srcSquare = null;
    }        

    private void border_DragEnter(object sender, DragEventArgs e)
    {        
        e.Effect = DragDropEffects.Move;
    }


    private void btnCreate_Click(object sender, EventArgs e)
    {

    }

    private void btnJoin_Click(object sender, EventArgs e)
    {
        //...
        rtxtChat.Enabled = true;
        txtMessage.Enabled = true;
        txtMessage.Select();
    }

    private void btnAI_Click(object sender, EventArgs e)
    {        
        foolsmate_resetGame();        
    }

    private void txtName_KeyUp(object sender, EventArgs e)
    {
        bool bEnabled = false;
        if (txtName.Text.Length > 1  &&  txtName.Text.Length < 16)  //activate Create, Join and AI sections
        {
            bEnabled = true;
        }
        pnlMode.Enabled = bEnabled;
    }

    private void btnMoveback_Click(object sender, EventArgs e)
    {
        foolsmate_takeBackMove();
    }

    private void btnSetPos_Click(object sender, EventArgs e)
    {
        if (null == m_positionSetter)
        {
            m_positionSetter = new CFPositionSetter();
        }
        m_positionSetter.ShowDialog(this);        
    }
}


}//namespace foolGUI
